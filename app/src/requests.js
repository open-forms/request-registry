import * as React from "react";
import { List, Datagrid, Edit, Create, SimpleForm, DateInput, DateField, TextField, EditButton, TextInput } from 'react-admin';
import BookIcon from '@material-ui/icons/Book';
export const RequestIcon = BookIcon;

export const RequestList = (props) => (
    <List {...props}>
        <Datagrid>
            <TextField source="id" />
            <TextField source="slug" />
            <DateField source="registrationDate" />
            <TextField source="form" />
            <TextField source="organization" />
            <TextField source="submitter" />
            <TextField source="status" />
            <TextField source="formInput" />
            <EditButton basePath="/requests" />
        </Datagrid>
    </List>
);
const RequestTitle = ({ record }) => {
    return <span>Request {record ? `"${record.id}"` : ''}</span>;
};

export const RequestEdit = (props) => (
    <Edit title={<RequestTitle />} {...props}>
        <SimpleForm>
            <TextInput disabled source="id" />
            <TextInput source="slug" />
            <DateInput source="registrationDate" />
            <TextInput source="form" />
            <TextInput source="organization" />
            <TextInput source="submitter" />
            <TextInput source="status" />
            <TextInput source="formInput" />
        </SimpleForm>
    </Edit>
);

export const RequestCreate = (props) => (
    <Create title="Create a Request" {...props}>
        <SimpleForm>
            <TextInput source="slug" />
            <DateInput source="registrationDate" />
            <TextInput source="form" />
            <TextInput source="organization" />
            <TextInput source="submitter" />
            <TextInput source="status" />
            <TextInput source="formInput" />
        </SimpleForm>
    </Create>
);