import * as React from "react";
import { render } from 'react-dom';
import { fetchUtils, Admin, Resource } from 'react-admin';
import jsonServerProvider from 'ra-data-json-server';

import { RequestList, RequestEdit, RequestCreate, RequestIcon } from './requests';


const httpClient = (url, options = {}) => {
    if (!options.headers) {
        options.headers = new Headers({ Accept: 'application/json' });
    }
    options.headers.set('Access-Control-Expose-Headers', 'X-Total-Count')
    return fetchUtils.fetchJson(url, options);
};
const dataProvider = jsonServerProvider('http://verzoeken.openformulieren.io', httpClient);

const App = () => (
    <Admin dataProvider={dataProvider}>
        <Resource name="requests" list={RequestList} edit={RequestEdit} create={RequestCreate} icon={RequestIcon}/>
    </Admin>
);

export default App;

render(
    <Admin dataProvider={dataProvider} title="Example Admin">
        <Resource name="requests" list={RequestList} edit={RequestEdit} create={RequestCreate} icon={RequestIcon}/>
    </Admin>,
    document.getElementById('root')
);