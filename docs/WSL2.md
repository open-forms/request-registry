# Configure development environment on Windows using WSL2

This document describes how to set up a `request-registry` development system using
WSL2 in Windows 10.

## Prerequisites

- [Windows Subsystem for Linux, version 2](https://docs.microsoft.com/en-us/windows/wsl/install-win10) (WSL2) is installed
- [Docker](https://docs.docker.com/docker-for-windows/install/) is installed and configured to be enabled in your WSL2 environment
- [Visual Studio Code](https://code.visualstudio.com/) is installed

## Clone repository

### Linux

It is recommended to work from inside a WSL2 environment.
Perform the following steps to clone the git repository.

From Windows, open a WSL2 prompt. You can do this from powershell, for example
if you have Ubuntu installed:

```
C:\work> ubuntu
```

You can also use the [Windows Terminal](https://www.microsoft.com/nl-nl/p/windows-terminal/9n0dx20hk701) app, which is able to launch WSL2 shells directly.

Clone the `request-registry` repository:

```bash
git clone git@gitlab.com:open-forms/request-registry.git
```

### Windows

Install git on Windows and use gitbash or an other prefered tool to clone the repository

```bash
git clone git@gitlab.com:open-forms/request-registry.git
```

Or use https (if 2FA is enabled)

```bash
git clone https://gitlab.com/open-forms/request-registry.git
```

## Configure IDE

### Linux

Open VSCode from inside WSL2.

```bash
$ code request-registry
```

Inside vscode, open a terminal and create a Python virtual environment using
the script `dev/create-venv.sh`. To have this work correctly, a C compiler and
development libraries need to be installed first (some Python modules we use
have components that are written in C):

```bash
$ sudo apt update
$ sudo apt-get install build-essential libpq-dev python3-venv python3-dev
$ cd dev
$ chmod 777 create-venv.sh
$ cd ..
$ dev/create-venv.sh
```

### Windows

For pythone open visual studio code and open the project directory

We assume Python is installed. If not see https://www.python.org/downloads/windows/

Inside VS code open a powershell terminal and create a .venv with the following commands:

Creating virtual environment in .venv

```powershell
PS C:\Work> python -m venv .venv
```

Activating venv

```powershell
PS C:\Work> .\.venv\Scripts\Activate.ps1
```

Install requirements

```powershell
PS C:\Work> pip3 install -r api/requirements.txt
```

## Start

You can now start the containers:

```bash
$ docker-compose build
$ docker-compose up -d
```

Make sure to update the db, the first time the app is started

```bash
docker-compose run --rm requests-api alembic upgrade head
```

The 'request-registry' should now be up and running!

Open some of the available url's to confirm the services are running -- see
[README.md](../README.md) in the root of this project for more:

- TODO
