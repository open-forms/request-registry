"""The main file"""

import copy
from datetime import timedelta
from typing import Any, Dict, List
import uvicorn
from fastapi import Depends, FastAPI, HTTPException, Request, status
from fastapi.encoders import jsonable_encoder
from fastapi.middleware.cors import CORSMiddleware
from fastapi.exceptions import RequestValidationError
from fastapi.responses import JSONResponse
from fastapi_jwt_auth import AuthJWT
from fastapi_jwt_auth.exceptions import AuthJWTException
from passlib.context import CryptContext
from pydantic import BaseModel  # remove this import.
from app.api.api_v1.routers import requests
from app.core import config
from app.schemas.user_schemas import User, UserResponse

# FastAPI Settings
app = FastAPI(
    title=config.PROJECT_NAME,
    docs_url="/api/docs",
    redoc_url="/api/redoc",
    openapi_url="/api",
)

# CORS Settings
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
    expose_headers=["X-Total-Count", "Content-Range"],
)


@app.exception_handler(RequestValidationError)
async def validation_exception_handler(request: Request, exc: RequestValidationError):
    """Catches validation exceptions from database validation"""
    # Convert default format to desired format where 'loc' is not a tuple but
    # a dict: loc: { 'source': 'body', 'field': 'name'}. See CTECO-94

    errors: List[Dict[str, Any]] = copy.deepcopy(exc.errors())
    for i, error in enumerate(exc.errors()):
        loc = error["loc"]
        if isinstance(loc, tuple) and len(loc) == 2:
            new_loc: Dict[str, Any] = {"source": loc[0], "field": loc[1]}
            errors[i]["loc"] = new_loc

    return JSONResponse(
        status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
        content=jsonable_encoder({"detail": errors}),
    )

# authentication:

private_key: str = f"""
-----BEGIN RSA PRIVATE KEY-----
{config.PRIVATE_KEY}
-----END RSA PRIVATE KEY-----
"""
public_key: str = f"""
-----BEGIN PUBLIC KEY-----
{config.PUBLIC_KEY}
-----END PUBLIC KEY-----
"""


class Settings(BaseModel):
    """Settings for the jwt authentication"""
    authjwt_algorithm: str = "RS512"
    authjwt_public_key: str = public_key
    authjwt_private_key: str = private_key
    # Configure application to store and get JWT from cookies
    authjwt_token_location: set = {"cookies"}
    # Disable CSRF Protection for this example. default is True
    authjwt_cookie_csrf_protect: bool = False


@AuthJWT.load_config  # type: ignore
def get_config():
    """Returns the authentication settings"""
    return Settings()


@app.exception_handler(AuthJWTException)
def authjwt_exception_handler(request: Request, exc: AuthJWTException):
    """This handler will catch AuthJWTExceptions, so when, for example,
    no auth is given while it is required this will trigger."""
    headers: dict = {}
    if request.method == "GET" and request.url == f"{config.APP_URL}/requests":
        headers = {
          "Content-Range": "results */0",
          "X-Total-Count": "results */0"
        }
    return JSONResponse(
        status_code=exc.status_code, content={"detail": exc.message}, headers=headers  # type: ignore # noqa: E501
    )


pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def get_password_hash(password: str):
    """Can be used to generate a password hash for a user password"""
    return pwd_context.hash(password)


def verify_password(plain_password: str, hashed_password: str):
    """Verifies a password"""
    return pwd_context.verify(plain_password, hashed_password)


# A fake database with users that can be used for authentication
fake_users_database = {
    "main+testadmin@conduction.nl": {
        "username": "main+testadmin@conduction.nl",
        "hashed_password": "$2b$12$XWNxppezIaeYlcvFOlov7u9DlNGogtrGc2XknX9/.IZC/2fYQz3GO",
    },
    "main+testuser@conduction.nl": {
        "username": "main+testuser@conduction.nl",
        "hashed_password": "$2b$12$kTep4HmATYR6Alf.KErSau5MwXI4ED5xfeGM6tg1nMxj8tZ0mVz/S",
    }
}


def get_user(database: dict, username: str):
    """Gets a user from a (fake) database"""
    if username in database:
        user_dict = database[username]
        return user_dict
    return False


def authenticate_user(input_user: User):
    """Authenticates a user by comparing input with the (fake) user database"""
    user = get_user(fake_users_database, input_user.username)
    if not user:
        return False
    if not verify_password(input_user.password, user.get('hashed_password')):
        return False
    return user


@app.post("/token")
async def login_for_access_token(input_user: User, authorize: AuthJWT = Depends()):
    """Generates a bearer jwt token if correct user authentication is given"""
    user = authenticate_user(input_user)  # type: ignore
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )

    expire_minutes = 15
    expires = timedelta(minutes=expire_minutes)
    additional_claims: dict = {}  # Example: {"client_id": "8f18a6ab-f3c5-436a-aec7-15b32619c091"}
    access_token = authorize.create_access_token(
        subject=input_user.username, expires_time=expires, user_claims=additional_claims
    )
    authorize.set_access_cookies(access_token)
    return {"access_token": access_token, "token_type": "bearer", "expire_time_minutes": expire_minutes}


@app.get("/users/me/", response_model=UserResponse)
async def read_users_me(authorize: AuthJWT = Depends()):
    """Gets the current user info (username only) using the bearer token"""
    authorize.jwt_required()

    credentials_exception = HTTPException(
      status_code=status.HTTP_401_UNAUTHORIZED,
      detail="Could not validate credentials",
      headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        username: str = authorize.get_jwt_subject()
        if username is None:
            raise credentials_exception
    except Exception as exception:
        raise credentials_exception from exception
    user = get_user(fake_users_database, username)
    if user is None:
        raise credentials_exception
    return user

# end authentication


PREFIX = ""

# Routers
app.include_router(
    requests.router,
    prefix=PREFIX,
    tags=["Requests"],
)

if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", reload=True, port=80)
