"""Module for creating initial data for the database"""
#!/usr/bin/env python3

from sqlalchemy.orm import Session
from app.database import models
from app.database.session import SessionLocal


def truncate_tables() -> None:
    """Truncate tables"""

    database: Session = SessionLocal()

    database.execute('truncate table "request" cascade')

    database.commit()


def create_initial_data() -> None:
    """Creates initial data for database"""
    database: Session = SessionLocal()

    request1 = models.Request(
        id="c16ae581-8d6f-424e-bff3-3a70a1374f0a",
        url="http://verzoeken.openformulieren.io/requests/"
        + "c16ae581-8d6f-424e-bff3-3a70a1374f0a",
        registrationDate="18-06-2021",
        form="{}",
        organization=None,
        submitter=None,
        status="INACTIVE",
        formInput="{}",
    )
    request2 = models.Request(
        id="69c08406-3984-4344-9b6d-6f4adec2a4b7",
        url="http://verzoeken.openformulieren.io/requests/"
        + "69c08406-3984-4344-9b6d-6f4adec2a4b7",
        registrationDate="07-10-2020",
        form="{}",
        organization=None,
        submitter=None,
        status="INACTIVE",
        formInput="{}",
    )

    database.add(request1)
    database.add(request2)

    database.flush()
    database.commit()


if __name__ == "__main__":
    print("First remove any existing data")
    truncate_tables()
    print("Creating initial data")
    create_initial_data()
    print("Initial data created")
