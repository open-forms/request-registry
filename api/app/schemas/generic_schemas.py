"""Module for the generic schema"""

import uuid as py_uuid
from typing import Optional
from pydantic import BaseModel


class FilterableId(BaseModel):
    """FilterableId"""
    id: Optional[py_uuid.UUID]
