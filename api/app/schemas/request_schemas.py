"""Module for the request schema"""

from __future__ import annotations

import uuid as py_uuid
from typing import Optional
import datetime
from pydantic import BaseModel, Field
from app.core.types import SortOrder
from app.database.models import Status
from app.schemas.generic_schemas import FilterableId
from app.schemas.to_optional import to_optional


class RequestBase(BaseModel):
    """Request base model"""
    form: str = Field(..., title="The form of the request")
    organization: Optional[str] = None
    submitter: Optional[str] = None
    status: Status = Field(..., title="The status of the request")
    formInput: str = Field(..., title="The formInput of the request")


class RequestIn(RequestBase):
    """Request used to validate user input body for create api call"""
    registrationDate: str = Field(..., title="The registrationDate (string) of the request")


class RequestCreate(RequestBase):
    """Request used to validate body for creating the actual request"""
    registrationDate: datetime.date = Field(..., title="The registrationDate of the request")
    url: str = Field(..., title="The url of the request")


class Request(RequestCreate):
    """Properties to return to client as response body"""
    id: py_uuid.UUID = Field(..., title="The unique uuid of the request")

    class Config:
        """Config for Request"""
        orm_mode = True
        allow_population_by_field_name = True


class RequestUpdate(to_optional(RequestIn)):  # type: ignore
    """Make fields optional to support partial update (patch)"""
    pass


RequestUpdate.update_forward_refs()


class RequestFilterableFields(FilterableId):
    """Class for the fields that are filterable"""
    registrationDate: Optional[str]

    class Config:
        """Config for RequestFilterableFields"""
        extra = "forbid"


class RequestSortableFields(BaseModel):
    """Class for the fields that are sortable"""
    id: Optional[SortOrder]
    registrationDate: Optional[SortOrder]
    status: Optional[SortOrder]

    class Config:
        """Config for RequestSortableFields"""
        extra = "forbid"
