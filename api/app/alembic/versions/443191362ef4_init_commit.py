"""init commit

Revision ID: 443191362ef4
Revises:
Create Date: 2021-07-07 13:39:48.908941+02:00

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql
from enum import Enum
import uuid
import datetime

# revision identifiers, used by Alembic.
revision = '443191362ef4'
down_revision = None
branch_labels = None
depends_on = None


class Status(str, Enum):
    """Status enum"""
    ACTIVE = "ACTIVE"
    INACTIVE = "INACTIVE"


def upgrade():
    """Upgrade"""
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('request',
                    sa.Column('id', postgresql.UUID(as_uuid=True), nullable=False, default=uuid.uuid4),
                    sa.Column('url', sa.String(), nullable=False),
                    sa.Column('registrationDate', sa.DateTime(), nullable=False, default=datetime.date),
                    sa.Column('form', sa.String(), nullable=False),
                    sa.Column('organization', sa.String(), nullable=True),
                    sa.Column('submitter', sa.String(), nullable=True),
                    sa.Column('status', sa.Enum(Status), nullable=False),
                    sa.Column('formInput', sa.String(), nullable=False),
                    sa.PrimaryKeyConstraint('id'),
                    )
    # ### end Alembic commands ###


def downgrade():
    """Downgrade"""
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('request')
    # ### end Alembic commands ###
