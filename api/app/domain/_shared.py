"""Module for filtering and sorting get functions"""

from copy import copy
from typing import Optional, Tuple
import dateutil.parser
import sqlalchemy as sa
from sqlalchemy.orm import Session
from sqlalchemy.sql.expression import distinct
from app.core.config import MAX_PAGE_SIZE
from app.core.types import (
    FilterOptions,
    RangeOptions,
    SearchResult,
    SortOptions,
    SortOrder,
    UnsignedInt,
)
from app.database import models


def get_list(
    database: Session,
    model,
    query: sa.sql.expression.Select,
    sort_options: Optional[SortOptions],
    range_options: Optional[RangeOptions],
    filter_options: Optional[FilterOptions],
    return_scalars=True,
) -> SearchResult:
    """
    Perform a search, following the "Simple REST" API for sorting, filtering
    and paging.
    """

    # First apply filters to list query
    query = __get_list_apply_filter(query, model, filter_options)

    # Count total number of results before applying the LIMIT
    total_results = (
        database.execute(
            query.with_only_columns(sa.func.count(distinct(model.id)))
            .order_by(None)
            .group_by(None)
        )
        .scalars()
        .one()
    )

    # Apply sorting
    query = __get_list_apply_sort(query, model, sort_options)

    query, start = __get_list_apply_range(query, range_options)

    result = (
        database.execute(query).scalars().all()
        if return_scalars
        else database.execute(query).all()
    )

    # Last result sequence number = start + number of results - 1
    # (start at 0 + 25 results - 1 = sequence number of last result = 24)
    range_end = start + len(result) - 1

    return SearchResult(
        result=result,
        start=start,
        end=range_end if range_end >= 0 else None,
        total=total_results,
    )


def __get_list_apply_sort(
    query: sa.sql.expression.Select,
    model,
    sort_options: Optional[SortOptions],
) -> sa.sql.expression.Select:
    if sort_options:
        if sort_options.model_name is not None:
            # Add a join to the query based on the model
            query = query.outerjoin(getattr(model, sort_options.model_name))
            # Now get the model from models and get the field from that model
            sort_column = getattr(
                getattr(models, sort_options.model_name.capitalize()),
                sort_options.field,
            )
            query = query.add_columns(sort_column)
        else:
            try:
                sort_column = getattr(model, sort_options.field)
                sort_column = (
                    sort_column.DESC()
                    if sort_options.order == SortOrder.DESC
                    else sort_column.ASC()
                )
            except Exception as exception:  # noqa E722
                # Add colum as text order
                sort_column = (
                    sa.text(f'"{sort_options.field}" desc')
                    if sort_options.order == SortOrder.DESC
                    else sa.text(f'"{sort_options.field}" asc')
                )

    else:
        sort_column = getattr(model, model.default_sort_field, "sid").asc()

    return query.order_by(sort_column)


def __get_list_apply_range(
    query: sa.sql.expression.Select, range_options: Optional[RangeOptions]
) -> Tuple[sa.sql.expression.Select, UnsignedInt]:
    if range_options:
        start = range_options.start
        end = range_options.end
    else:
        start = UnsignedInt(0)
        end = UnsignedInt(MAX_PAGE_SIZE - 1)

    limit = end - start
    offset = start

    query = query.limit(limit).offset(offset)

    return query, start


def __build_fulltext_search(model, search_term: str):
    """
    Build a full-text search filter for a given model and search term.

    Will use `ILIKE` and escape .
    """
    search_term = (
        search_term.replace("/", "//").replace("%", "/%").replace("_", "/_")
    )

    fulltext_terms = []
    for fulltext_field in model.fulltext_fields:
        fulltext_terms.append(
            getattr(model, fulltext_field).ilike(f"%{search_term}%", escape="/")
        )

    return sa.or_(*fulltext_terms)


def __get_list_apply_filter(
    query: sa.sql.expression.Select,
    model,
    filter_options: Optional[FilterOptions],
) -> sa.sql.expression.Select:
    """
    Apply the filters specified in `filter_options` to `query` and return the
    new `query`.

    The filters specified are combined with "and" (they all need to match),
    and the values need to match the exactly.

    The "q" filter is special: it performs a full text search (currently
    implemented as an ILIKE) on the "full text" fields specified in the model.

    The "createdAt_gte" filter is special: it filters on the created_at and
    checks if the value is greather or eaqueal than the value provided

    The "createdAt_lte" filter is special: it filters on the created_at and
    checks if the value is less or equeal than the value provided

    The "is_active" filter is also special: if it is specified and `false`,
    inactive (i.e. where 'now' is not between `started` and `ended`) records
    are returned. By default, or if `is_active` is `true`, inactive records
    will be filtered out.
    """
    if not filter_options:
        return query

    options = copy(filter_options.options)

    try:
        active_only = bool(options.pop("is_active"))
    except KeyError:
        # If "is active" is not specified, default to only returning
        # active rows.
        active_only = True

    if active_only and hasattr(model, "started") and hasattr(model, "ended"):
        query = query.filter(
            sa.func.tstzrange(model.started, model.ended).op("@>")(
                sa.func.current_timestamp()
            )
        )

    for key, value in options.items():
        if key == "q":
            if not hasattr(model, "fulltext_fields"):
                raise TypeError("No search fields defined resource")

            query = query.filter(__build_fulltext_search(model, str(value)))
        elif key[-4:] == "_gte":
            query = query.filter(
                getattr(model, key[0:-4]) >= dateutil.parser.parse(value)
            )
        elif key[-4:] == "_lte":
            query = query.filter(
                getattr(model, key[0:-4]) <= dateutil.parser.parse(value)
            )
        else:
            if isinstance(value, list):
                query = query.filter(getattr(model, key).in_(value))
            else:
                query = query.filter(getattr(model, key) == value)

    return query
