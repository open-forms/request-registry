"""Base CRUD module"""

import uuid
from typing import Any, Dict, Generic, Optional, Type, TypeVar, Union
import sqlalchemy as sa
from fastapi.encoders import jsonable_encoder
from pydantic import BaseModel
from sqlalchemy.orm import Session
from app.core.types import (
    FilterOptions,
    RangeOptions,
    SearchResult,
    SemanticError,
    SemanticErrorType,
    SortOptions,
)
from app.domain._shared import get_list
from .base_class import Base

ModelType = TypeVar("ModelType", bound=Base)
CreateSchemaType = TypeVar("CreateSchemaType", bound=BaseModel)
UpdateSchemaType = TypeVar("UpdateSchemaType", bound=BaseModel)


class CRUDBase(Generic[ModelType, CreateSchemaType, UpdateSchemaType]):
    """The CRUD Base class"""
    def __init__(self, model: Type[ModelType]):
        """
        CRUD object with default methods to Create, Read, Update, Delete(CRUD)

        **Parameters**

        * `model`: A SQLAlchemy model class
        * `schema`: A Pydantic model (schema) class
        """
        self.model = model

    def get(
        self,
        database: Session,
        id: Union[uuid.UUID],
    ) -> Optional[ModelType]:
        """Basic CRUD get item call"""

        if isinstance(id, uuid.UUID):
            query = database.query(self.model).filter(self.model.id == id)

        result = database.execute(query).scalars().first()

        if not result:
            raise SemanticError(
                loc={"source": "path", "field": "id"},
                msg="Record not found",
                type=SemanticErrorType.NOT_FOUND,
            )

        return result

    def get_multi(
        self,
        database: Session,
        sort_options: Optional[SortOptions],
        range_options: Optional[RangeOptions],
        filter_options: Optional[FilterOptions],
    ) -> SearchResult:
        """Basic CRUD get collection call"""

        query = sa.select(self.model)  # type: ignore

        return get_list(
            database=database,
            model=self.model,
            query=query,
            sort_options=sort_options,
            range_options=range_options,
            filter_options=filter_options,
        )

    def create(
        self,
        database: Session,
        obj_in: CreateSchemaType,
    ) -> ModelType:
        """Basic CRUD create call"""

        obj_in_data = obj_in.dict(exclude_unset=True)
        database_obj = self.model(**obj_in_data)  # type: ignore

        try:
            database.add(database_obj)
            database.flush()  # generate uuid used in audit log
            database.commit()
        except sa.exc.IntegrityError as exception:
            raise SemanticError(
                loc={"source": "body", "field": "x"},
                msg="Unable to create Record, ...",
                type=SemanticErrorType.NOT_UNIQUE,
            ) from exception
        database.refresh(database_obj)
        return database_obj

    def update(
        self,
        database: Session,
        *,
        id: Union[uuid.UUID],
        obj_in: Union[UpdateSchemaType, Dict[str, Any]],
    ) -> Union[ModelType, None]:
        """Basic CRUD update call"""

        database_obj = self.get(database=database, id=id)
        obj_data = jsonable_encoder(database_obj)

        if isinstance(obj_in, dict):
            update_data = obj_in
        else:
            update_data = obj_in.dict(exclude_unset=True)
        for field in obj_data:  # type: ignore
            if field in update_data:
                setattr(database_obj, field, update_data[field])
        try:
            database.add(database_obj)
            database.commit()
        except sa.exc.IntegrityError as exception:
            raise SemanticError(
                loc={"source": "body", "field": "x"},
                msg="Unable to update Record, ...",
                type=SemanticErrorType.NOT_UNIQUE,
            ) from exception
        database.refresh(database_obj)
        return database_obj

    def delete(
        self,
        database: Session,
        *,
        id: Union[uuid.UUID],
    ) -> ModelType:
        """Basic CRUD delete call"""

        if isinstance(id, uuid.UUID):
            obj = database.query(self.model).get(id)
            if not obj:
                raise SemanticError(
                    loc={"source": "path", "field": "id"},
                    msg="Confict: Record already deleted",
                    type=SemanticErrorType.ALREADY_DELETED,
                )

        database.delete(obj)
        database.commit()
        return obj
