"""Module for request CRUD calls"""

from app import schemas
from app.database import models
from app.domain.base import CRUDBase


class CRUDRequest(CRUDBase[models.Request, schemas.RequestCreate, schemas.RequestCreate]):
    """Class for request CRUD calls"""
    pass


request = CRUDRequest(models.Request)
