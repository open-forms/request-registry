import base64
import datetime
from datetime import timedelta
from fastapi.testclient import TestClient
from fastapi_jwt_auth import AuthJWT

# The api endpoint
url = "/requests"


# ---------------------------------------------
# Test post request without an authorization header.
# ---------------------------------------------
def test_post_request_without_auth_header(
    client: TestClient,
):

    body = {
        "registrationDate": str(datetime.date.today()),
        "form": "http://formulieren.openformulieren.io/forms/918deb7d-364c-4124-a67a-d23590d34bc3",  # type: ignore # noqa: E501
        "organization": "c16ae581-8d6f-424e-bff3-3a70a1374f0a",
        "submitter": "main+testadmin@conduction.nl",
        "status": "ACTIVE",
        "formInput": "{}",
    }

    response = client.post(
        url,
        json=body,
    )

    # TODO 401 “Unauthorized” for requests that do not contain a proper bearer token # type: ignore # noqa: E501
    assert response.status_code == 401
    res = response.json()
    assert len(res) == 1
    assert res["detail"] == "Missing Authorization Header"


# ---------------------------------------------
# Test post request with an invalid Bearer token.
# ---------------------------------------------
def test_post_request_with_invalid_bearer_token(
    client: TestClient,
):

    headers = {
        "Authorization": "Bearer blabla",
    }

    body = {
        "registrationDate": str(datetime.date.today()),
        "form": "http://formulieren.openformulieren.io/forms/918deb7d-364c-4124-a67a-d23590d34bc3",  # type: ignore # noqa: E501
        "organization": "c16ae581-8d6f-424e-bff3-3a70a1374f0a",
        "submitter": "main+testadmin@conduction.nl",
        "status": "ACTIVE",
        "formInput": "{}",
    }

    response = client.post(
        url,
        headers=headers,
        json=body,
    )

    # TODO 401 “Unauthorized” for requests that do not contain a proper bearer token # type: ignore # noqa: E501
    assert response.status_code == 422
    res = response.json()
    assert len(res) == 1
    assert res["detail"] == "Not enough segments"


# ---------------------------------------------
# Test post request with ahttp basic authentication
# ---------------------------------------------
def test_post_request_with_http_basic_auth(
    client: TestClient,
):

    encoded = base64.b64encode(b"tim:tester")
    headers = {
        "Authorization": "Basic {!r}".format(encoded),
    }

    body = {
        "registrationDate": str(datetime.date.today()),
        "form": "http://formulieren.openformulieren.io/forms/918deb7d-364c-4124-a67a-d23590d34bc3",  # type: ignore # noqa: E501
        "organization": "c16ae581-8d6f-424e-bff3-3a70a1374f0a",
        "submitter": "main+testadmin@conduction.nl",
        "status": "ACTIVE",
        "formInput": '{"tEst":"","name":"bestaat niet","lastName":"","watIsUwVraag":""}',  # type: ignore # noqa: E501
    }

    response = client.post(
        url,
        headers=headers,
        json=body,
    )

    # TODO 401 “Unauthorized” for requests that do not contain a proper bearer token # type: ignore # noqa: E501
    assert response.status_code == 422
    res = response.json()
    print(res)
    assert len(res) == 1
    assert (
        res["detail"]
        == "Bad Authorization header. Expected value 'Bearer <JWT>'"
    )


# ---------------------------------------------
# Test post request with an expiraton claim in the past.
# ---------------------------------------------
def test_post_request_with_exp_claim_in_the_past(
    client: TestClient,
    authorize: AuthJWT,
):

    # Test HTTP authentication with an expiration time ("exp" claim) in the past. # type: ignore # noqa: E501
    expires = timedelta(minutes=-1)
    additional_claims: dict = {}
    access_token = authorize.create_access_token(
        subject="", expires_time=expires, user_claims=additional_claims
    )

    headers = {
        "Authorization": "Bearer {}".format(access_token),
    }

    body = {
        "registrationDate": str(datetime.date.today()),
        "form": "http://formulieren.openformulieren.io/forms/918deb7d-364c-4124-a67a-d23590d34bc3",  # type: ignore # noqa: E501
        "organization": "c16ae581-8d6f-424e-bff3-3a70a1374f0a",
        "submitter": "main+testadmin@conduction.nl",
        "status": "ACTIVE",
        "formInput": "{}",
    }

    response = client.post(
        url,
        headers=headers,
        json=body,
    )

    # TODO 401 “Unauthorized” for requests that do not contain a proper bearer token # type: ignore # noqa: E501
    assert response.status_code == 422
    res = response.json()
    assert len(res) == 1
    assert res["detail"] == "Signature has expired"
