import datetime
import pytest
from fastapi.testclient import TestClient
from unittest import mock

# The api endpoint
url = "/requests"


# ---------------------------------------------
# Test post request without a request content type.
# The body contains JSON data.
# Content type is not mandatory.
# A sender that generates a message containing a payload body SHOULD generate
# a Content-Type header field in that message unless the intended media type
# of the enclosed representation is unknown to the sender.
# ---------------------------------------------
# TODO Reject requests containing unexpected or missing content type headers
# with HTTP response status 406 Unacceptable or 415 Unsupported Media Type.
def test_post_request_without_request_header_content_type(
    client: TestClient,
    get_form_request_function: dict,
    http_authorization_bearer_access_token: dict,
):

    mock_patch = "app.services.validationService.get_resource"

    with mock.patch(mock_patch) as mck:
        mck.side_effect = get_form_request_function

        body = {
            "registrationDate": str(datetime.date.today()),
            "form": "http://formulieren.openformulieren.io/forms/918deb7d-364c-4124-a67a-d23590d34bc3",  # type: ignore # noqa: E501
            "organization": "c16ae581-8d6f-424e-bff3-3a70a1374f0a",
            "submitter": "main+testadmin@conduction.nl",
            "status": "ACTIVE",
            "formInput": '{"tEst":"","name":"bestaat niet","lastName":"","watIsUwVraag":""}',  # type: ignore # noqa: E501
        }

        response = client.post(
            url,
            headers=http_authorization_bearer_access_token,
            json=body,
        )

        assert response.status_code == 201  # TODO 406 of 415
        res = response.json()
        assert len(res) == 8
        assert res["id"] is not None
        assert res["url"] is not None
        assert res["registrationDate"] == str(datetime.date.today())
        assert res["form"] == body["form"]
        assert res["organization"] == body["organization"]
        assert res["submitter"] == body["submitter"]
        assert res["status"] == body["status"]
        assert res["formInput"] == body["formInput"]

        # Only allow response type application/json.
        response_headers = response.headers
        assert len(response_headers) == 2
        assert response_headers["content-type"] == "application/json"
        assert response_headers["content-length"] is not None


# ---------------------------------------------
# Test post request with a request content type application/json.
# The body contains JSON data.
# ---------------------------------------------
# TODO Reject the request (ideally with a 406 Not Acceptable response) if the
# Accept header does not contain the allowed type application/json.
@pytest.mark.parametrize(
    "http_request_header_accept",
    [
        "application/javascript",
        "application/json",  # allowed
        "application/octet-stream",
        "multipart/form-data",
        "text/html",
        "text/plain",
        "not valid",
        "application/*",
        "*/*",
        "",
        None,
    ],
)
def test_post_request_with_request_header_content_type_application_json(
    client: TestClient,
    get_form_request_function: dict,
    http_authorization_bearer_access_token: dict,
    http_request_header_accept: str,
):

    mock_patch = "app.services.validationService.get_resource"

    headers = {
        "Accept": f"{http_request_header_accept}",
        "Content-Type": "application/json",
    }
    headers.update(http_authorization_bearer_access_token)

    with mock.patch(mock_patch) as mck:
        mck.side_effect = get_form_request_function

        body = {
            "registrationDate": str(datetime.date.today()),
            "form": "http://formulieren.openformulieren.io/forms/918deb7d-364c-4124-a67a-d23590d34bc3",  # type: ignore # noqa: E501
            "organization": "c16ae581-8d6f-424e-bff3-3a70a1374f0a",
            "submitter": "main+testadmin@conduction.nl",
            "status": "ACTIVE",
            "formInput": '{"tEst":"","name":"bestaat niet","lastName":"","watIsUwVraag":""}',  # type: ignore # noqa: E501
        }

        response = client.post(
            url,
            headers=headers,
            json=body,
        )

        assert response.status_code == 201  # TODO 406
        res = response.json()
        assert len(res) == 8
        assert res["id"] is not None
        assert res["url"] is not None
        assert res["registrationDate"] == str(datetime.date.today())
        assert res["form"] == body["form"]
        assert res["organization"] == body["organization"]
        assert res["submitter"] == body["submitter"]
        assert res["status"] == body["status"]
        assert res["formInput"] == body["formInput"]

        # Only allow response type application/json.
        response_headers = response.headers
        assert len(response_headers) == 2
        assert response_headers["content-type"] == "application/json"
        assert response_headers["content-length"] is not None


# ---------------------------------------------
# Test post request with an invalid request content type.
# The body contains JSON data.
# The Content-Type header attribute specifies the format of the data in the
# request body so that receiver can parse it into appropriate format.
# ---------------------------------------------
# TODO Reject requests containing unexpected or missing content type headers
# with HTTP response status 406 Unacceptable or 415 Unsupported Media Type.
@pytest.mark.parametrize(
    "http_request_header_content_type",
    [
        "application/javascript",
        "application/octet-stream",
        "multipart/form-data",
        "text/html",
        "text/plain",
        "",
        "not valid",
        None,
    ],
)
def test_post_request_with_request_header_content_type_with_invalid_value(
    client: TestClient,
    http_authorization_bearer_access_token: dict,
    http_request_header_content_type: str,
):

    headers = {
        "Content-Type": f"{http_request_header_content_type}",
    }
    headers.update(http_authorization_bearer_access_token)

    body = {
        "registrationDate": str(datetime.date.today()),
        "form": "http://formulieren.openformulieren.io/forms/918deb7d-364c-4124-a67a-d23590d34bc3",  # type: ignore # noqa: E501
        "organization": "c16ae581-8d6f-424e-bff3-3a70a1374f0a",
        "submitter": "main+testadmin@conduction.nl",
        "status": "ACTIVE",
        "formInput": '{"tEst":"","name":"bestaat niet","lastName":"","watIsUwVraag":""}',  # type: ignore # noqa: E501
    }

    response = client.post(
        url,
        headers=headers,
        json=body,
    )

    assert response.status_code == 400  # 406 or 415
    res = response.json()
    assert len(res) == 1
    assert len(res["detail"]) == 1
    assert len(res["detail"][0]) == 3
    assert res["detail"][0]["name"] == "Bad Request"
    assert res["detail"][0]["message"] == "RequestValidationError"
    assert len(res["detail"][0]["stack"]) == 2

    # Only allow response type application/json.
    response_headers = response.headers
    assert len(response_headers) == 2
    assert response_headers["content-type"] == "application/json"
    assert response_headers["content-length"] is not None
