import datetime
from datetime import timedelta
from fastapi.testclient import TestClient
from requests.models import Response
from unittest import mock
from unittest.mock import Mock

# The api endoint
url = "/requests"

# TODO: use dummy data instead of 'real' data


# ---------------------------------------------
# Test post request with an empty body.
# ---------------------------------------------
def test_post_request_with_empty_body(
    client: TestClient,
    http_authorization_bearer_access_token: dict,
):

    body: dict = {}

    response = client.post(
        url,
        json=body,
        headers=http_authorization_bearer_access_token,
    )

    assert response.status_code == 400  # TODO 422
    res = response.json()
    assert len(res) == 1
    assert len(res["detail"]) == 1
    assert res["detail"][0]["name"] == "Bad Request"
    assert res["detail"][0]["message"] == "RequestValidationError"


# ---------------------------------------------
# Test post request with an invalid registrationDate.
# The value is empty.
# TODO: test registrationDate in the past
# TODO: test registrationDate in the future
# ---------------------------------------------
def test_post_request_registration_date_is_empty(
    client: TestClient,
    http_authorization_bearer_access_token: dict,
):

    body = {
        "registrationDate": "",
        "form": "http://formulieren.openformulieren.io/forms/918deb7d-364c-4124-a67a-d23590d34bc3",  # type: ignore # noqa: E501
        "organization": "c16ae581-8d6f-424e-bff3-3a70a1374f0a",
        "submitter": "main+testadmin@conduction.nl",
        "status": "ACTIVE",
        "formInput": "{}",
    }

    response = client.post(
        url,
        headers=http_authorization_bearer_access_token,
        json=body,
    )

    assert response.status_code == 400
    res = response.json()
    assert len(res) == 1
    assert len(res["detail"]) == 1
    assert len(res["detail"][0]) == 3
    assert res["detail"][0]["name"] == "Bad Request"
    assert (
        res["detail"][0]["message"]
        == "registrationDate:  is geen geldige datum, geef een geldige datum op, die een d-m-Y of Y-m-d formaat heeft."  # type: ignore # noqa: E501
    )
    assert len(res["detail"][0]["stack"]) == 0


# ---------------------------------------------
# Test post request with an invalid registrationDate.
# The value is None. None is not an allowed value.
# ---------------------------------------------
def test_post_request_registration_date_is_none(
    client: TestClient,
    http_authorization_bearer_access_token: dict,
):

    body = {
        "registrationDate": None,
        "form": "http://formulieren.openformulieren.io/forms/918deb7d-364c-4124-a67a-d23590d34bc3",  # type: ignore # noqa: E501
        "organization": "c16ae581-8d6f-424e-bff3-3a70a1374f0a",
        "submitter": "main+testadmin@conduction.nl",
        "status": "ACTIVE",
        "formInput": '{"tEst":"","name":"bestaat niet","lastName":"","watIsUwVraag":""}',  # type: ignore # noqa: E501
    }

    response = client.post(
        url,
        headers=http_authorization_bearer_access_token,
        json=body,
    )

    assert response.status_code == 400
    res = response.json()
    assert len(res) == 1
    assert len(res["detail"]) == 1
    assert len(res["detail"][0]) == 3
    assert res["detail"][0]["name"] == "Bad Request"
    assert res["detail"][0]["message"] == "RequestValidationError"
    assert len(res["detail"][0]["stack"]) == 2


# ---------------------------------------------
# Test post request with an invalid registrationDate.
# The format is not valid.
# ---------------------------------------------
def test_post_request_registration_date_with_invalid_format(
    client: TestClient,
    http_authorization_bearer_access_token: dict,
):
    body = {
        "registrationDate": "06/09/2021",
        "form": "http://formulieren.openformulieren.io/forms/918deb7d-364c-4124-a67a-d23590d34bc3",  # type: ignore # noqa: E501
        "organization": "c16ae581-8d6f-424e-bff3-3a70a1374f0a",
        "submitter": "main+testadmin@conduction.nl",
        "status": "ACTIVE",
        "formInput": '{"tEst":"","name":"bestaat niet","lastName":"","watIsUwVraag":""}',  # type: ignore # noqa: E501
    }

    response = client.post(
        url,
        headers=http_authorization_bearer_access_token,
        json=body,
    )

    assert response.status_code == 400
    res = response.json()
    assert len(res) == 1
    assert len(res["detail"]) == 1
    assert len(res["detail"][0]) == 3
    assert res["detail"][0]["name"] == "Bad Request"
    assert (
        res["detail"][0]["message"]
        == "registrationDate: 06/09/2021 is geen geldige datum, geef een geldige datum op, die een d-m-Y of Y-m-d formaat heeft."  # type: ignore # noqa: E501
    )
    assert len(res["detail"][0]["stack"]) == 0


# ---------------------------------------------
# Test post request with an invalid form value.
# ---------------------------------------------
def test_post_request_invalid_form_value(
    client: TestClient,
    http_authorization_bearer_access_token: dict,
):

    # Test form values with an invalid url.
    for formValue in [
        "http://formulieren.openformulieren.io/forms/918deb7d-364c-4124-a67a-d23590d34bc3-11",  # type: ignore # noqa: E501
        "https://google.nl",
        "https://formulieren.openformulieren.io/forms/918deb7d-364c-4124-a67a-d23590d34bc3",  # type: ignore # noqa: E501
    ]:
        body = {
            "registrationDate": str(datetime.date.today()),
            "form": formValue,
            "organization": "c16ae581-8d6f-424e-bff3-3a70a1374f0a",
            "submitter": "main+testadmin@conduction.nl",
            "status": "ACTIVE",
            "formInput": '{"tEst":"","name":"bestaat niet","lastName":"","watIsUwVraag":""}',  # type: ignore # noqa: E501
        }

        response = client.post(
            url,
            headers=http_authorization_bearer_access_token,
            json=body,
        )

        assert response.status_code == 400
        res = response.json()
        assert len(res) == 1
        assert len(res["detail"]) == 1
        assert len(res["detail"][0]) == 3
        assert res["detail"][0]["name"] == "Bad Request"
        assert (
            res["detail"][0]["message"]
            == f"form: {formValue} is geen geldige url, geef een geldige url op voor een formulier object: http://formulieren.openformulieren.io/forms/[uuid]."  # type: ignore # noqa: E501
        )
        assert len(res["detail"][0]["stack"]) == 1
        assert res["detail"][0]["stack"]["type"] == "invalidForm"

    # Test form values with an invalid value and value length <= 100.
    for formValue in [
        "",
        "__",
        '{"name": "formulier a","comp":""}',
        "{}",
        "[]",
        "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890",  # type: ignore # noqa: E501
    ]:
        body = {
            "registrationDate": str(datetime.date.today()),
            "form": formValue,
            "organization": "c16ae581-8d6f-424e-bff3-3a70a1374f0a",
            "submitter": "main+testadmin@conduction.nl",
            "status": "ACTIVE",
            "formInput": '{"tEst":"","name":"bestaat niet","lastName":"","watIsUwVraag":""}',  # type: ignore # noqa: E501
        }

        response = client.post(
            url,
            headers=http_authorization_bearer_access_token,
            json=body,
        )

        assert response.status_code == 400
        res = response.json()
        assert len(res) == 1
        assert len(res["detail"]) == 1
        assert len(res["detail"][0]) == 3
        assert res["detail"][0]["name"] == "Bad Request"
        assert (
            res["detail"][0]["message"]
            == f"form: {formValue} is geen geldige url of geldige form.io form (json)."  # type: ignore # noqa: E501
        )
        assert len(res["detail"][0]["stack"]) == 1
        assert res["detail"][0]["stack"]["type"] == "invalidForm"

    # Test form values with an invalid value and value length > 100.
    for formValue in [
        "12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901",  # type: ignore # noqa: E501
        "12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678902",  # type: ignore # noqa: E501
    ]:
        body = {
            "registrationDate": str(datetime.date.today()),
            "form": formValue,
            "organization": "c16ae581-8d6f-424e-bff3-3a70a1374f0a",
            "submitter": "main+testadmin@conduction.nl",
            "status": "ACTIVE",
            "formInput": '{"tEst":"","name":"bestaat niet","lastName":"","watIsUwVraag":""}',  # type: ignore # noqa: E501
        }

        response = client.post(
            url,
            headers=http_authorization_bearer_access_token,
            json=body,
        )

        assert response.status_code == 400
        res = response.json()
        assert len(res) == 1
        assert len(res["detail"]) == 1
        assert len(res["detail"][0]) == 3
        assert res["detail"][0]["name"] == "Bad Request"
        assert (
            res["detail"][0]["message"]
            == "form: form is geen geldige url of geldige form.io form (json)."  # type: ignore # noqa: E501
        )
        assert len(res["detail"][0]["stack"]) == 1
        assert res["detail"][0]["stack"]["type"] == "invalidForm"

    # Test invalid form value.
    # TODO: waarom wordt een form zonder URL en met 'components' wel geaccepteerd? # type: ignore # noqa: E501
    for formValue in [
        '{"name": "formulier a","components":""}',
        '{"components":""}',
    ]:
        body = {
            "registrationDate": str(datetime.date.today()),
            "form": formValue,
            "organization": "c16ae581-8d6f-424e-bff3-3a70a1374f0a",
            "submitter": "main+testadmin@conduction.nl",
            "status": "ACTIVE",
            "formInput": '{"tEst":"","name":"bestaat niet","lastName":"","watIsUwVraag":""}',  # type: ignore # noqa: E501
        }

        response = client.post(
            url,
            headers=http_authorization_bearer_access_token,
            json=body,
        )

        assert response.status_code == 201
        res = response.json()
        assert len(res) == 8
        assert res["id"] is not None
        assert res["url"] is not None
        assert res["registrationDate"] == body["registrationDate"]
        assert res["form"] == body["form"]
        assert res["organization"] == body["organization"]
        assert res["submitter"] == body["submitter"]
        assert res["status"] == body["status"]
        assert res["formInput"] == body["formInput"]


# ---------------------------------------------
# Test post request with an unknown form.
# ---------------------------------------------
def test_post_request_unknown_form_resource(
    client: TestClient,
    mocker: Mock,
    http_authorization_bearer_access_token: dict,
):
    # This is the api return value.
    api_return_value = ""

    response = Mock(spec=Response)
    response.json.return_value = api_return_value
    response.status_code = 200

    mocker.patch(
        "app.services.validationService.get_resource",
        return_value=response,
    )

    body = {
        "registrationDate": str(datetime.date.today()),
        "form": "http://formulieren.openformulieren.io/forms/918deb7d-364c-4124-a67a-d23590d34bc3",  # type: ignore # noqa: E501
        "organization": "c16ae581-8d6f-424e-bff3-3a70a1374f0a",
        "submitter": "main+testadmin@conduction.nl",
        "status": "ACTIVE",
        "formInput": '{"tEst":"","name":"bestaat niet","lastName":"","watIsUwVraag":""}',  # type: ignore # noqa: E501
    }

    response = client.post(
        url,
        headers=http_authorization_bearer_access_token,
        json=body,
    )

    assert response.status_code == 400
    res = response.json()
    assert len(res) == 1
    assert len(res["detail"]) == 1
    assert len(res["detail"][0]) == 3
    assert res["detail"][0]["name"] == "Bad Request"
    assert (
        res["detail"][0]["message"]
        == f"form: {body['form']} is geen bestaand formulier object in de forms-catalogue."  # type: ignore # noqa: E501
    )  # form.io form is geen geldige json
    assert len(res["detail"][0]["stack"]) == 1
    assert res["detail"][0]["stack"]["type"] == "invalidForm"


# ---------------------------------------------
# Test post request with an inactive form.
# ---------------------------------------------
def test_post_request_inactive_form_resource(
    client: TestClient,
    mocker: Mock,
    http_authorization_bearer_access_token: dict,
):

    # This is the api return value.
    # It contains an inactive form (status is inactive).
    api_return_value = {
        "name": "formulier a",
        "slug": "formulier-a",
        "url": "http://formulieren.openformulieren.io/forms/918deb7d-364c-4124-a67a-d23590d34bc3",  # type: ignore # noqa: E501
        "status": "INACTIVE",
        "form": {},
        "config": "",
        "organizationId": "c16ae581-8d6f-424e-bff3-3a70a1374f0a",
        "organizationUrl": "",
        "publishDate": str(datetime.date.today()),
        "id": "918deb7d-364c-4124-a67a-d23590d34bc3",
    }

    response = Mock(spec=Response)
    response.json.return_value = api_return_value
    response.status_code = 200

    mocker.patch(
        "app.services.validationService.get_resource",
        return_value=response,
    )

    body = {
        "registrationDate": str(datetime.date.today()),
        "form": "http://formulieren.openformulieren.io/forms/918deb7d-364c-4124-a67a-d23590d34bc3",  # type: ignore # noqa: E501
        "organization": "c16ae581-8d6f-424e-bff3-3a70a1374f0a",
        "submitter": "main+testadmin@conduction.nl",
        "status": "ACTIVE",
        "formInput": '{"tEst":"","name":"bestaat niet","lastName":"","watIsUwVraag":""}',  # type: ignore # noqa: E501
    }

    response = client.post(
        url,
        headers=http_authorization_bearer_access_token,
        json=body,
    )

    assert response.status_code == 400
    res = response.json()
    assert len(res) == 1
    assert len(res["detail"]) == 1
    assert len(res["detail"][0]) == 3
    assert res["detail"][0]["name"] == "Bad Request"
    assert (
        res["detail"][0]["message"]
        == f"form: {body['form']} kan niet ingediend worden. De status van het formulier is ‘inactief’. Indien u het formulier in wilt kunnen dienen dient de status door de beheerder veranderd te worden naar 'actief’."  # type: ignore # noqa: E501
    )
    assert len(res["detail"][0]["stack"]) == 1
    assert res["detail"][0]["stack"]["type"] == "invalidForm"


# ---------------------------------------------
# Test post request with an form where the publishDate is in the future
# ---------------------------------------------
def test_post_request_form_with_publishdate_in_the_future(
    client: TestClient,
    mocker: Mock,
    http_authorization_bearer_access_token: dict,
):

    # This is the api return value.
    # It contains a publishDate in the future.
    api_return_value = {
        "name": "formulier a",
        "slug": "formulier-a",
        "url": "http://formulieren.openformulieren.io/forms/918deb7d-364c-4124-a67a-d23590d34bc3",  # type: ignore # noqa: E501
        "status": "ACTIVE",
        "form": {},
        "config": "",
        "organizationId": "c16ae581-8d6f-424e-bff3-3a70a1374f0a",
        "organizationUrl": "",
        "publishDate": str(datetime.date.today() + timedelta(days=365)),
        "id": "918deb7d-364c-4124-a67a-d23590d34bc3",
    }

    response = Mock(spec=Response)
    response.json.return_value = api_return_value
    response.status_code = 200

    mocker.patch(
        "app.services.validationService.get_resource",
        return_value=response,
    )

    body = {
        "registrationDate": str(datetime.date.today()),
        "form": "http://formulieren.openformulieren.io/forms/918deb7d-364c-4124-a67a-d23590d34bc3",  # type: ignore # noqa: E501
        "organization": "c16ae581-8d6f-424e-bff3-3a70a1374f0a",
        "submitter": "main+testadmin@conduction.nl",
        "status": "ACTIVE",
        "formInput": '{"tEst":"","name":"bestaat niet","lastName":"","watIsUwVraag":""}',  # type: ignore # noqa: E501
    }

    response = client.post(
        url,
        headers=http_authorization_bearer_access_token,
        json=body,
    )

    assert response.status_code == 400
    res = response.json()
    assert len(res) == 1
    assert len(res["detail"]) == 1
    assert len(res["detail"][0]) == 3
    assert res["detail"][0]["name"] == "Bad Request"
    assert (
        res["detail"][0]["message"]
        == f"form: {body['form']} kan niet ingediend worden. Het formulier is nog niet gepubliceerd."  # type: ignore # noqa: E501
    )
    assert len(res["detail"][0]["stack"]) == 1
    assert res["detail"][0]["stack"]["type"] == "invalidForm"


# ---------------------------------------------
# Test post request with a valid form value.
# ---------------------------------------------
def test_post_request_valid_form_value(
    client: TestClient,
    get_form_request_function: dict,
    http_authorization_bearer_access_token: dict,
):

    mock_patch = "app.services.validationService.get_resource"

    # Test 1
    with mock.patch(mock_patch) as mck:
        mck.side_effect = get_form_request_function

        body = {
            "registrationDate": "06-09-2021",
            "form": "http://formulieren.openformulieren.io/forms/918deb7d-364c-4124-a67a-d23590d34bc3",  # type: ignore # noqa: E501
            "organization": "c16ae581-8d6f-424e-bff3-3a70a1374f0a",
            "submitter": "main+testadmin@conduction.nl",
            "status": "ACTIVE",
            "formInput": '{"tEst":"","name":"bestaat niet","lastName":"","watIsUwVraag":""}',  # type: ignore # noqa: E501
        }

        response = client.post(
            url,
            headers=http_authorization_bearer_access_token,
            json=body,
        )

        assert response.status_code == 201
        res = response.json()
        assert len(res) == 8
        assert res["id"] is not None
        assert res["url"] is not None
        assert res["registrationDate"] == "2021-09-06"
        assert res["form"] == body["form"]
        assert res["organization"] == body["organization"]
        assert res["submitter"] == body["submitter"]
        assert res["status"] == body["status"]
        assert res["formInput"] == body["formInput"]

    # Test 2
    with mock.patch(mock_patch) as mck:
        mck.side_effect = get_form_request_function

        body = {
            "registrationDate": str(datetime.date.today()),
            "form": "http://formulieren.openformulieren.io/forms/918deb7d-364c-4124-a67a-d23590d34bc3",  # type: ignore # noqa: E501
            "organization": "c16ae581-8d6f-424e-bff3-3a70a1374f0a",
            "submitter": "main+testadmin@conduction.nl",
            "status": "ACTIVE",
            "formInput": '{"tEst":"","name":"bestaat niet","lastName":"","watIsUwVraag":""}',  # type: ignore # noqa: E501
        }

        response = client.post(
            url,
            headers=http_authorization_bearer_access_token,
            json=body,
        )

        assert response.status_code == 201
        res = response.json()
        assert len(res) == 8
        assert res["id"] is not None
        assert res["url"] is not None
        assert res["registrationDate"] == body["registrationDate"]
        assert res["form"] == body["form"]
        assert res["organization"] == body["organization"]
        assert res["submitter"] == body["submitter"]
        assert res["status"] == body["status"]
        assert res["formInput"] == body["formInput"]


# ---------------------------------------------
# Test a http post request with an invalid (formio) form.
# ---------------------------------------------
def test_post_request_invalid_form_resource(
    client: TestClient,
    mocker: Mock,
    http_authorization_bearer_access_token: dict,
):

    # This is the api return value.
    # It contains an invalid (formio) form.
    api_return_value = {
        "name": "formulier a",
        "slug": "formulier-a",
        "url": "http://formulieren.openformulieren.io/forms/918deb7d-364c-4124-a67a-d23590d34bc3",  # type: ignore # noqa: E501
        "status": "ACTIVE",
        "form": {
            "display": "wizard",
            "title": "formtitel",
            "name": "formname",
            "type": ["form"],
            "path": "formpath",
            "components": [
                {
                    "title": "Page 1",
                }
            ],
        },
        "config": "",
        "organizationId": "c16ae581-8d6f-424e-bff3-3a70a1374f0a",
        "organizationUrl": "",
        "publishDate": str(datetime.date.today()),
        "id": "918deb7d-364c-4124-a67a-d23590d34bc3",
    }

    response = Mock(spec=Response)
    response.json.return_value = api_return_value
    response.status_code = 200

    mocker.patch(
        "app.services.validationService.get_resource",
        return_value=response,
    )

    body = {
        "registrationDate": str(datetime.date.today()),
        "form": "http://formulieren.openformulieren.io/forms/918deb7d-364c-4124-a67a-d23590d34bc3",  # type: ignore # noqa: E501
        "organization": "c16ae581-8d6f-424e-bff3-3a70a1374f0a",
        "submitter": "main+testadmin@conduction.nl",
        "status": "ACTIVE",
        "formInput": '{"tEst":"","name":"bestaat niet","lastName":"","watIsUwVraag":""}',  # type: ignore # noqa: E501
    }

    response = client.post(
        url,
        headers=http_authorization_bearer_access_token,
        json=body,
    )

    assert response.status_code == 400
    res = response.json()
    assert len(res) == 1
    assert len(res["detail"]) == 1
    assert len(res["detail"][0]) == 3
    assert res["detail"][0]["name"] == "Bad Request"
    assert (
        res["detail"][0]["message"]
        == f"{body['form']} form.io form is geen geldige json."
    )
    assert len(res["detail"][0]["stack"]) == 1
    assert res["detail"][0]["stack"]["type"] == "invalidForm"
