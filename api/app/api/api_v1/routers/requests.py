"""Module for all request api routes"""

import uuid
from datetime import datetime
from typing import List, Union
from fastapi import APIRouter, Depends, Response, status
from fastapi.responses import JSONResponse
from fastapi.exceptions import HTTPException
from fastapi_jwt_auth import AuthJWT
from app import domain, schemas
from app.core import config
from app.core.rest_helpers import SearchParameters
from app.core.types import SemanticError, SemanticErrorType
from app.database.session import get_database
from app.services.notification_service import notify, NotifyActions
from app.services.validation_service import ValidationService

validation_service = ValidationService()

router = r = APIRouter()

validate_search_parameters = SearchParameters(
    filter_options_schema=schemas.RequestFilterableFields,  # type: ignore
    sort_options_schema=schemas.RequestSortableFields,  # type: ignore
)


@r.get("/")
async def root():
    """The root path and entrypoint for this api, showing the subpath /requests"""
    return {
        "@context": "/contexts/Entrypoint",
        "@id": "/",
        "@type": "Entrypoint",
        "openapi": "/api",
        "docs": "/api/docs",
        "redoc": "/api/redoc",
        "request": "/requests",
    }


@r.get(
    "/requests/{id}",
    response_model=schemas.Request,
    responses={404: {"description": "Request not found"}},
)
def get_request(
    id: Union[uuid.UUID],
    database=Depends(get_database),
    authorize: AuthJWT = Depends(),
):
    """Retrieve a single custom field, given its unique id"""
    authorize.jwt_required()
    try:
        request = domain.request.get(database=database, id=id)
    except SemanticError as semantic_error:
        error_content = [{
            "name": "Not Found",
            "message": f"Er is geen verzoek gevonden met {id}",
            "stack": {"type": SemanticErrorType.NOT_FOUND}
        }]
        raise HTTPException(status_code=404, detail=error_content) from semantic_error

    return request


@r.get("/requests", response_model=List[schemas.Request])
def get_requests(
    response: Response,
    database=Depends(get_database),
    search_parameters: dict = Depends(validate_search_parameters),
    authorize: AuthJWT = Depends(),
):
    """Retrieves a list of requests"""

    # example of reading token additional_claims:
    # client_id = authorize.get_raw_jwt()['client_id']
    # print(client_id)

    authorize.jwt_required()

    search_result = domain.request.get_multi(database=database, **search_parameters)

    response.headers["Content-Range"] = search_result.as_content_range(
        "results"
    )
    response.headers["X-Total-Count"] = search_result.as_content_range(
        "results"
    )

    return search_result.result


def validate_datetime_string(datetime_str: str):
    """Check if given string is a valid date string and return a valid datetime object or false"""
    try:
        registration_date = datetime.strptime(datetime_str, '%d-%m-%Y')
    except ValueError:
        try:
            registration_date = datetime.strptime(datetime_str, '%Y-%m-%d')
        except ValueError:
            return False
    return registration_date


@r.post(
    "/requests",
    response_model=schemas.Request,
    status_code=201,
    responses={
        400: {"description": "Bad Request. Request not created."},
        500: {"description": "Internal Server Error. Request not created."},
    },
)
def create_request(
    request_in: schemas.RequestIn,
    database=Depends(get_database),
):
    """Creates a new request"""

    registration_date = validate_datetime_string(request_in.registrationDate)
    if not registration_date:
        error_content = [{
            "name": "Bad Request",
            "message": f"registrationDate: {request_in.registrationDate} is geen geldige datum, geef een geldige "
                       f"datum op, die een d-m-Y of Y-m-d formaat heeft.",
            "stack": {}
        }]
        raise HTTPException(status_code=400, detail=error_content)

    validation_result = validation_service.validate_request(request_in, database)
    if validation_result is not True:
        return JSONResponse(content={"detail": validation_result}, status_code=status.HTTP_400_BAD_REQUEST)

    try:
        # create the request
        request = domain.request.create(
            database=database,
            obj_in=schemas.RequestCreate(
                registrationDate=registration_date,
                form=request_in.form,
                organization=request_in.organization,
                submitter=request_in.submitter,
                status=request_in.status,
                formInput=request_in.formInput,
                url="placeholder",
            ),
        )
        # set the url for this request with its new uuid
        request.url = f"{config.APP_URL}/requests/{request.id}"
        # update the newly created request
        request_result = domain.request.update(
            database=database,
            id=request.id,
            obj_in=schemas.RequestUpdate(url=request.url),
        )
    except Exception as ex:
        template = "Arguments:{1!r}"
        error_content = [{
            "name": "Internal Server Error",
            "message": "Verzoek is niet aangemaakt. Er ging iets mis.",
            "stack": {"type": type(ex).__name__, "args": template.format('', ex.args)}
        }]
        raise HTTPException(status_code=500, detail=error_content) from ex

    # notify with notification_service:
    if request_result is not None:
        notify("requests", NotifyActions.CREATE, str(request_result.id))
    return request_result


@r.put(
    "/requests/{id}",
    response_model=schemas.Request,
    responses={
        404: {"description": "Request not found"},
        500: {"description": "Internal Server Error. Request not updated."},
    },
)
def update_request(
    id: Union[uuid.UUID],
    request: schemas.RequestUpdate,
    database=Depends(get_database),
    authorize: AuthJWT = Depends(),
):
    """Update a request"""

    authorize.jwt_required()
    try:
        domain.request.get(database=database, id=id)
    except SemanticError as semantic_error:
        error_content = [{
            "name": "Not Found",
            "message": f"Er is geen verzoek gevonden met {id}",
            "stack": {"type": SemanticErrorType.NOT_FOUND}
        }]
        raise HTTPException(status_code=404, detail=error_content) from semantic_error

    registration_date = validate_datetime_string(request.registrationDate)
    if not registration_date:
        error_content = [{
            "name": "Bad Request",
            "message": f"registrationDate: {request.registrationDate} is geen geldige datum, geef een geldige datum "
                       f"op, die een d-m-Y of Y-m-d formaat heeft.",
            "stack": {}
        }]
        raise HTTPException(status_code=400, detail=error_content)
    request.registrationDate = registration_date

    validation_result = validation_service.validate_request(request, database)
    if validation_result is not True:
        return JSONResponse(content={"detail": validation_result}, status_code=status.HTTP_400_BAD_REQUEST)
        # should this be a 400 bad request?

    # update the request
    try:
        updated_request = domain.request.update(
            database=database,
            id=id,
            obj_in=request,
        )
    except Exception as ex:
        template = "Arguments:{1!r}"
        error_content = [{
            "name": "Internal Server Error",
            "message": "Verzoek is niet gewijzigd. Er ging iets mis.",
            "stack": {"type": type(ex).__name__, "args": template.format('', ex.args)}
        }]
        raise HTTPException(status_code=500, detail=error_content) from ex

    # notify with notification_service:
    notify("requests", NotifyActions.UPDATE, str(id))
    return updated_request


@r.delete(
    "/requests/{id}",
    responses={
        404: {"description": "Request not found"},
        500: {"description": "Internal Server Error. Request not deleted."},
    },
)
def delete_request(
    id: Union[uuid.UUID],
    database=Depends(get_database),
    authorize: AuthJWT = Depends(),
):
    """Delete request"""

    authorize.jwt_required()
    try:
        request = domain.request.get(database=database, id=id)
    except SemanticError as semantic_error:
        error_content = [{
            "name": "Not Found",
            "message": f"Er is geen verzoek gevonden met {id}",
            "stack": {"type": SemanticErrorType.NOT_FOUND}
        }]
        raise HTTPException(status_code=404, detail=error_content) from semantic_error

    if request is None:
        error_content = [
            {
                "name": "Not Found",
                "message": f"Er is geen verzoek gevonden met {id}",
                "stack": {"type": SemanticErrorType.NOT_FOUND},
            }
        ]
        raise HTTPException(status_code=404, detail=error_content)

    try:
        domain.request.delete(database=database, id=request.id)
    except Exception as ex:
        template = "Arguments:{1!r}"
        error_content = [{
            "name": "Internal Server Error",
            "message": "Verzoek is niet verwijderd. Er ging iets mis.",
            "stack": {"type": type(ex).__name__, "args": template.format('', ex.args)}
        }]
        raise HTTPException(status_code=500, detail=error_content) from ex

    # notify with notification_service:
    notify("requests", NotifyActions.DELETE, str(id))
    return {"message": f"Verzoek met id: {id} is verwijderd."}
