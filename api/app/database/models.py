"""This module contains the database models"""

from enum import Enum
import uuid
import datetime
import sqlalchemy as sa
import sqlalchemy.dialects.postgresql as pg
from .session import Base


class Status(str, Enum):
    """Status enum"""
    ACTIVE = "ACTIVE"
    INACTIVE = "INACTIVE"


class Request(Base):
    """The request table in the database"""

    __tablename__ = "request"
    default_sort_field = "registrationDate"

    id = sa.Column(
        pg.UUID(as_uuid=True),
        primary_key=True,
        default=uuid.uuid4
    )
    url = sa.Column(sa.String, nullable=True)
    registrationDate = sa.Column(sa.DateTime, default=datetime.date)
    form = sa.Column(sa.String)
    organization = sa.Column(sa.String, nullable=True)
    submitter = sa.Column(sa.String, nullable=True)
    status = sa.Column(sa.Enum(Status))
    formInput = sa.Column(sa.String)
