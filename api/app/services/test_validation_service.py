"""This module is used for testing the validation_service"""

import json
from unittest.mock import Mock
import pytest
from requests.models import Response
from app.services.validation_service import ValidationService


def test_get_form_properties_mock():
    """Test the get form properties."""

    # This is the api return value.
    # It contains a valid (formio) form.
    api_return_value = {
        "name": "formulier a",
        "slug": "formulier-a",
        "url": "http://formulieren.openformulieren.io/forms/918deb7d-364c-4124-a67a-d23590d34bc3",
        # type: ignore # noqa: E501 ^
        "status": "ACTIVE",
        "form": '{"display":"wizard","title":"formtitel","name":"formname","type":["form"],"path":"formpath",'
                '"components":[{"title":"Page 1","label":"Page 1","type":"panel","key":"page1","components":'
                '[{"label":"Vraag","labelPosition":"top","placeholder":"","description":"","tooltip":"","prefix":"",'
                '"suffix":"","widget":{"type":"input"},"inputMask":"","allowMultipleMasks":false,"customClass":"",'
                '"tabindex":"","autocomplete":"","hidden":false,"hideLabel":false,"showWordCount":false,"showCharCount"'
                ':false,"mask":false,"autofocus":false,"spellcheck":true,"disabled":false,"tableView":true,"modalEdit":'
                'false,"multiple":false,"persistent":true,"inputFormat":"plain","protected":false,"dbIndex":false,'
                '"case":"","encrypted":false,"redrawOn":"","clearOnHide":true,"customDefaultValue":"","calculateValue"'
                ':"","calculateServer":false,"allowCalculateOverride":false,"validateOn":"change","validate":'
                '{"required":false,"pattern":"","customMessage":"","custom":"","customPrivate":false,"json":"",'
                '"minLength":"","maxLength":"","strictDateValidation":false,"multiple":false,"unique":false},"unique":'
                'false,"errorLabel":"","key":"tEst","tags":[],"properties":{},"conditional":{"show":null,"when":null,'
                '"eq":"","json":""},"customConditional":"","logic":[],"attributes":{},"overlay":{"style":"","page":"",'
                '"left":"","top":"","width":"","height":""},"type":"textfield","input":true,"refreshOn":"",'
                '"dataGridLabel":false,"inputType":"text","id":"emvmmhk","defaultValue":""},{"label":"Voornaam",'
                '"labelPosition":"top","placeholder":"","description":"","tooltip":"","prefix":"","suffix":"",'
                '"widget":{"type":"input"},"inputMask":"","allowMultipleMasks":false,"customClass":"","tabindex":"",'
                '"autocomplete":"","hidden":false,"hideLabel":false,"showWordCount":false,"showCharCount":false,"mask":'
                'false,"autofocus":false,"spellcheck":true,"disabled":false,"tableView":true,"modalEdit":false,'
                '"multiple":false,"persistent":true,"inputFormat":"plain","protected":false,"dbIndex":false,"case":"",'
                '"encrypted":false,"redrawOn":"","clearOnHide":true,"customDefaultValue":"","calculateValue":"",'
                '"calculateServer":false,"allowCalculateOverride":false,"validateOn":"change","validate":{"required":'
                'true,"pattern":"","customMessage":"","custom":"","customPrivate":false,"json":"","minLength":"",'
                '"maxLength":"","strictDateValidation":false,"multiple":false,"unique":false},"unique":false,'
                '"errorLabel":"","key":"name","tags":[],"properties":{},"conditional":{"show":null,"when":null,"eq":"",'
                '"json":""},"customConditional":"","logic":[],"attributes":{},"overlay":{"style":"","page":"","left":'
                '"","top":"","width":"","height":""},"type":"textfield","input":true,"refreshOn":"","dataGridLabel":'
                'false,"inputType":"text","id":"evzldlun","defaultValue":""},{"label":"Achternaam","tableView":true,'
                '"key":"lastName","type":"textfield","input":true,"placeholder":"","prefix":"","customClass":"",'
                '"suffix":"","multiple":false,"defaultValue":null,"protected":false,"unique":false,"persistent":true,'
                '"hidden":false,"clearOnHide":true,"refreshOn":"","redrawOn":"","modalEdit":false,"labelPosition":'
                '"top","description":"","errorLabel":"","tooltip":"","hideLabel":false,"tabindex":"","disabled":false,'
                '"autofocus":false,"dbIndex":false,"customDefaultValue":"","calculateValue":"","calculateServer":false,'
                '"widget":{"type":"input"},"attributes":{},"validateOn":"change","validate":{"required":false,"custom":'
                '"","customPrivate":false,"strictDateValidation":false,"multiple":false,"unique":false,"minLength":"",'
                '"maxLength":"","pattern":""},"conditional":{"show":null,"when":null,"eq":""},"overlay":{"style":"",'
                '"left":"","top":"","width":"","height":""},"allowCalculateOverride":false,"encrypted":false,'
                '"showCharCount":false,"showWordCount":false,"properties":{},"allowMultipleMasks":false,"mask":false,'
                '"inputType":"text","inputFormat":"plain","inputMask":"","spellcheck":true,"id":"een8nx9",'
                '"dataGridLabel":false}],"input":false,"placeholder":"","prefix":"","customClass":"","suffix":"",'
                '"multiple":false,"defaultValue":null,"protected":false,"unique":false,"persistent":false,"hidden":'
                'false,"clearOnHide":false,"refreshOn":"","redrawOn":"","tableView":false,"modalEdit":false,'
                '"labelPosition":"top","description":"","errorLabel":"","tooltip":"","hideLabel":false,"tabindex":"",'
                '"disabled":false,"autofocus":false,"dbIndex":false,"customDefaultValue":"","calculateValue":"",'
                '"calculateServer":false,"widget":null,"attributes":{},"validateOn":"change","validate":{"required":'
                'false,"custom":"","customPrivate":false,"strictDateValidation":false,"multiple":false,"unique":false},'
                '"conditional":{"show":null,"when":null,"eq":""},"overlay":{"style":"","left":"","top":"","width":"",'
                '"height":""},"allowCalculateOverride":false,"encrypted":false,"showCharCount":false,"showWordCount":'
                'false,"properties":{},"allowMultipleMasks":false,"tree":false,"theme":"default","breadcrumb":'
                '"default","id":"eypah2g","dataGridLabel":false},{"title":"Page 2","label":"Page 2","type":"panel",'
                '"key":"page2","components":[{"label":"Wat is uw vraag","labelPosition":"top","placeholder":"",'
                '"description":"","tooltip":"","prefix":"","suffix":"","widget":{"type":"input"},"editor":"",'
                '"autoExpand":false,"customClass":"","tabindex":"","autocomplete":"","hidden":false,"hideLabel":false,'
                '"showWordCount":false,"showCharCount":false,"autofocus":false,"spellcheck":true,"disabled":false,'
                '"tableView":true,"modalEdit":false,"multiple":false,"persistent":true,"inputFormat":"html",'
                '"protected":false,"dbIndex":false,"case":"","encrypted":false,"redrawOn":"","clearOnHide":true,'
                '"customDefaultValue":"","calculateValue":"","calculateServer":false,"allowCalculateOverride":false,'
                '"validateOn":"change","validate":{"required":false,"pattern":"","customMessage":"","custom":"",'
                '"customPrivate":false,"json":"","minLength":"","maxLength":"","minWords":"","maxWords":"",'
                '"strictDateValidation":false,"multiple":false,"unique":false},"unique":false,"errorLabel":"","key":'
                '"watIsUwVraag","tags":[],"properties":{},"conditional":{"show":null,"when":null,"eq":"","json":""},'
                '"customConditional":"","logic":[],"attributes":{},"overlay":{"style":"","page":"","left":"","top":"",'
                '"width":"","height":""},"type":"textarea","rows":3,"wysiwyg":false,"input":true,"refreshOn":"",'
                '"dataGridLabel":false,"allowMultipleMasks":false,"mask":false,"inputType":"text","inputMask":"",'
                '"fixedSize":true,"id":"exiwwsc","defaultValue":""}],"input":false,"placeholder":"","prefix":"",'
                '"customClass":"","suffix":"","multiple":false,"defaultValue":null,"protected":false,"unique":false,'
                '"persistent":false,"hidden":false,"clearOnHide":false,"refreshOn":"","redrawOn":"","tableView":false,'
                '"modalEdit":false,"dataGridLabel":false,"labelPosition":"top","description":"","errorLabel":"",'
                '"tooltip":"","hideLabel":false,"tabindex":"","disabled":false,"autofocus":false,"dbIndex":false,'
                '"customDefaultValue":"","calculateValue":"","calculateServer":false,"widget":null,"attributes":{},'
                '"validateOn":"change","validate":{"required":false,"custom":"","customPrivate":false,'
                '"strictDateValidation":false,"multiple":false,"unique":false},"conditional":{"show":null,"when":null,'
                '"eq":""},"overlay":{"style":"","left":"","top":"","width":"","height":""},"allowCalculateOverride":'
                'false,"encrypted":false,"showCharCount":false,"showWordCount":false,"properties":{},'
                '"allowMultipleMasks":false,"tree":false,"theme":"default","breadcrumb":"default","id":"ehcb46d"}]}',
                # type: ignore # noqa: E501 ^
        "config": "",
        "organizationId": "c16ae581-8d6f-424e-bff3-3a70a1374f0a",
        "organizationUrl": "",
        "publishDate": "2021-09-06",
        "id": "918deb7d-364c-4124-a67a-d23590d34bc3",
    }

    response = Mock(spec=Response)
    response.json.return_value = api_return_value
    response.status_code = 200
    assert response is not None

    response_value = response.json()
    assert response_value is not None
    assert isinstance(response_value, dict)

    form = response_value.get("form")
    assert form is not None
    assert isinstance(form, str)

    form_components: list = json.loads(form)["components"]
    assert form_components is not None
    assert isinstance(form_components, list)


@pytest.mark.parametrize(
    "field_value",
    [
        "test",
        "_",
        " __  ",
        "      ",
        None,
    ],
)
def test_validate_required_with_value(
    field_value: str,
):
    """Test required input fields."""

    # This contains a valid (formio) form, with three input fields.
    # Inputfield name and lastname are mandatory.
    form_data = {
        "form": '{"display":"wizard","title":"formtitel","name":"formname","type":["form"],"path":"formpath",'
                '"components":[{"title":"Page 1","label":"Page 1","type":"panel","key":"page1","components":[{"label":'
                '"Vraag","labelPosition":"top","placeholder":"","description":"","tooltip":"","prefix":"","suffix":"",'
                '"widget":{"type":"input"},"inputMask":"","allowMultipleMasks":false,"customClass":"","tabindex":"",'
                '"autocomplete":"","hidden":false,"hideLabel":false,"showWordCount":false,"showCharCount":false,"mask":'
                'false,"autofocus":false,"spellcheck":true,"disabled":false,"tableView":true,"modalEdit":false,'
                '"multiple":false,"persistent":true,"inputFormat":"plain","protected":false,"dbIndex":false,"case":"",'
                '"encrypted":false,"redrawOn":"","clearOnHide":true,"customDefaultValue":"","calculateValue":"",'
                '"calculateServer":false,"allowCalculateOverride":false,"validateOn":"change","validate":{"required":'
                'false,"pattern":"","customMessage":"","custom":"","customPrivate":false,"json":"","minLength":"",'
                '"maxLength":"","strictDateValidation":false,"multiple":false,"unique":false},"unique":false,'
                '"errorLabel":"","key":"tEst","tags":[],"properties":{},"conditional":{"show":null,"when":null,"eq":"",'
                '"json":""},"customConditional":"","logic":[],"attributes":{},"overlay":{"style":"","page":"","left":'
                '"","top":"","width":"","height":""},"type":"textfield","input":true,"refreshOn":"","dataGridLabel":'
                'false,"inputType":"text","id":"emvmmhk","defaultValue":""},{"label":"Voornaam","labelPosition":"top",'
                '"placeholder":"","description":"","tooltip":"","prefix":"","suffix":"","widget":{"type":"input"},'
                '"inputMask":"","allowMultipleMasks":false,"customClass":"","tabindex":"","autocomplete":"","hidden":'
                'false,"hideLabel":false,"showWordCount":false,"showCharCount":false,"mask":false,"autofocus":false,'
                '"spellcheck":true,"disabled":false,"tableView":true,"modalEdit":false,"multiple":false,"persistent":'
                'true,"inputFormat":"plain","protected":false,"dbIndex":false,"case":"","encrypted":false,"redrawOn":'
                '"","clearOnHide":true,"customDefaultValue":"","calculateValue":"","calculateServer":false,'
                '"allowCalculateOverride":false,"validateOn":"change","validate":{"required":true,"pattern":"",'
                '"customMessage":"","custom":"","customPrivate":false,"json":"","minLength":"","maxLength":"",'
                '"strictDateValidation":false,"multiple":false,"unique":false},"unique":false,"errorLabel":"","key":'
                '"name","tags":[],"properties":{},"conditional":{"show":null,"when":null,"eq":"","json":""},'
                '"customConditional":"","logic":[],"attributes":{},"overlay":{"style":"","page":"","left":"","top":"",'
                '"width":"","height":""},"type":"textfield","input":true,"refreshOn":"","dataGridLabel":false,'
                '"inputType":"text","id":"evzldlun","defaultValue":""},{"label":"Achternaam","tableView":true,"key":'
                '"lastName","type":"textfield","input":true,"placeholder":"","prefix":"","customClass":"","suffix":"",'
                '"multiple":false,"defaultValue":null,"protected":false,"unique":false,"persistent":true,"hidden":'
                'false,"clearOnHide":true,"refreshOn":"","redrawOn":"","modalEdit":false,"labelPosition":"top",'
                '"description":"","errorLabel":"","tooltip":"","hideLabel":false,"tabindex":"","disabled":false,'
                '"autofocus":false,"dbIndex":false,"customDefaultValue":"","calculateValue":"","calculateServer":false,'
                '"widget":{"type":"input"},"attributes":{},"validateOn":"change","validate":{"required":false,"custom":'
                '"","customPrivate":false,"strictDateValidation":false,"multiple":false,"unique":false,"minLength":"",'
                '"maxLength":"","pattern":""},"conditional":{"show":null,"when":null,"eq":""},"overlay":{"style":"",'
                '"left":"","top":"","width":"","height":""},"allowCalculateOverride":false,"encrypted":false,'
                '"showCharCount":false,"showWordCount":false,"properties":{},"allowMultipleMasks":false,"mask":false,'
                '"inputType":"text","inputFormat":"plain","inputMask":"","spellcheck":true,"id":"een8nx9",'
                '"dataGridLabel":false}],"input":false,"placeholder":"","prefix":"","customClass":"","suffix":"",'
                '"multiple":false,"defaultValue":null,"protected":false,"unique":false,"persistent":false,"hidden":'
                'false,"clearOnHide":false,"refreshOn":"","redrawOn":"","tableView":false,"modalEdit":false,'
                '"labelPosition":"top","description":"","errorLabel":"","tooltip":"","hideLabel":false,"tabindex":"",'
                '"disabled":false,"autofocus":false,"dbIndex":false,"customDefaultValue":"","calculateValue":"",'
                '"calculateServer":false,"widget":null,"attributes":{},"validateOn":"change","validate":{"required":'
                'false,"custom":"","customPrivate":false,"strictDateValidation":false,"multiple":false,"unique":false},'
                '"conditional":{"show":null,"when":null,"eq":""},"overlay":{"style":"","left":"","top":"","width":"",'
                '"height":""},"allowCalculateOverride":false,"encrypted":false,"showCharCount":false,"showWordCount":'
                'false,"properties":{},"allowMultipleMasks":false,"tree":false,"theme":"default","breadcrumb":'
                '"default","id":"eypah2g","dataGridLabel":false},{"title":"Page 2","label":"Page 2","type":"panel",'
                '"key":"page2","components":[{"label":"Wat is uw vraag","labelPosition":"top","placeholder":"",'
                '"description":"","tooltip":"","prefix":"","suffix":"","widget":{"type":"input"},"editor":"",'
                '"autoExpand":false,"customClass":"","tabindex":"","autocomplete":"","hidden":false,"hideLabel":false,'
                '"showWordCount":false,"showCharCount":false,"autofocus":false,"spellcheck":true,"disabled":false,'
                '"tableView":true,"modalEdit":false,"multiple":false,"persistent":true,"inputFormat":"html",'
                '"protected":false,"dbIndex":false,"case":"","encrypted":false,"redrawOn":"","clearOnHide":true,'
                '"customDefaultValue":"","calculateValue":"","calculateServer":false,"allowCalculateOverride":false,'
                '"validateOn":"change","validate":{"required":false,"pattern":"","customMessage":"","custom":"",'
                '"customPrivate":false,"json":"","minLength":"","maxLength":"","minWords":"","maxWords":"",'
                '"strictDateValidation":false,"multiple":false,"unique":false},"unique":false,"errorLabel":"","key":'
                '"watIsUwVraag","tags":[],"properties":{},"conditional":{"show":null,"when":null,"eq":"","json":""},'
                '"customConditional":"","logic":[],"attributes":{},"overlay":{"style":"","page":"","left":"","top":"",'
                '"width":"","height":""},"type":"textarea","rows":3,"wysiwyg":false,"input":true,"refreshOn":"",'
                '"dataGridLabel":false,"allowMultipleMasks":false,"mask":false,"inputType":"text","inputMask":"",'
                '"fixedSize":true,"id":"exiwwsc","defaultValue":""}],"input":false,"placeholder":"","prefix":"",'
                '"customClass":"","suffix":"","multiple":false,"defaultValue":null,"protected":false,"unique":false,'
                '"persistent":false,"hidden":false,"clearOnHide":false,"refreshOn":"","redrawOn":"","tableView":false,'
                '"modalEdit":false,"dataGridLabel":false,"labelPosition":"top","description":"","errorLabel":"",'
                '"tooltip":"","hideLabel":false,"tabindex":"","disabled":false,"autofocus":false,"dbIndex":false,'
                '"customDefaultValue":"","calculateValue":"","calculateServer":false,"widget":null,"attributes":{},'
                '"validateOn":"change","validate":{"required":false,"custom":"","customPrivate":false,'
                '"strictDateValidation":false,"multiple":false,"unique":false},"conditional":{"show":null,"when":null,'
                '"eq":""},"overlay":{"style":"","left":"","top":"","width":"","height":""},"allowCalculateOverride":'
                'false,"encrypted":false,"showCharCount":false,"showWordCount":false,"properties":{},'
                '"allowMultipleMasks":false,"tree":false,"theme":"default","breadcrumb":"default","id":"ehcb46d"}]}',
                # type: ignore # noqa: E501 ^
    }

    # The user input
    form_input = {"tEst": "test", "name": f"{field_value}", "lastName": "", "watIsUwVraag": ""}  # type: ignore # noqa: E501

    assert form_data is not None
    assert isinstance(form_data, dict)

    assert form_input is not None
    assert isinstance(form_input, dict)

    form = form_data.get("form", "")
    assert form is not None
    assert isinstance(form, str)

    form_components: list = json.loads(form)["components"]
    assert form_components is not None
    assert isinstance(form_components, list)

    validation_service = ValidationService()
    validation_service.errors = validation_service.Errors()
    validation_service.form_input_dict = form_input

    validation_service.validate_form_input_dict(form_components)
    assert len(validation_service.errors.get_errors()) == 0
    assert int(validation_service.errors.error_amount) == 0


@pytest.mark.parametrize(
    "field_empty_value",
    [
        "",
    ],
)
def test_validate_required_without_value(
    field_empty_value: str,
):
    """Test required input fields."""

    # This contains a valid (formio) form, with three input fields.
    # Inputfield name and lastname are mandatory.
    form_data = {
        "form": '{"display":"wizard","title":"formtitel","name":"formname","type":["form"],"path":"formpath",'
                '"components":[{"title":"Page 1","label":"Page 1","type":"panel","key":"page1","components":[{"label":'
                '"Vraag","labelPosition":"top","placeholder":"","description":"","tooltip":"","prefix":"","suffix":"",'
                '"widget":{"type":"input"},"inputMask":"","allowMultipleMasks":false,"customClass":"","tabindex":"",'
                '"autocomplete":"","hidden":false,"hideLabel":false,"showWordCount":false,"showCharCount":false,"mask":'
                'false,"autofocus":false,"spellcheck":true,"disabled":false,"tableView":true,"modalEdit":false,'
                '"multiple":false,"persistent":true,"inputFormat":"plain","protected":false,"dbIndex":false,"case":"",'
                '"encrypted":false,"redrawOn":"","clearOnHide":true,"customDefaultValue":"","calculateValue":"",'
                '"calculateServer":false,"allowCalculateOverride":false,"validateOn":"change","validate":{"required":'
                'false,"pattern":"","customMessage":"","custom":"","customPrivate":false,"json":"","minLength":"",'
                '"maxLength":"","strictDateValidation":false,"multiple":false,"unique":false},"unique":false,'
                '"errorLabel":"","key":"tEst","tags":[],"properties":{},"conditional":{"show":null,"when":null,"eq":"",'
                '"json":""},"customConditional":"","logic":[],"attributes":{},"overlay":{"style":"","page":"","left":'
                '"","top":"","width":"","height":""},"type":"textfield","input":true,"refreshOn":"","dataGridLabel":'
                'false,"inputType":"text","id":"emvmmhk","defaultValue":""},{"label":"Voornaam","labelPosition":"top",'
                '"placeholder":"","description":"","tooltip":"","prefix":"","suffix":"","widget":{"type":"input"},'
                '"inputMask":"","allowMultipleMasks":false,"customClass":"","tabindex":"","autocomplete":"","hidden":'
                'false,"hideLabel":false,"showWordCount":false,"showCharCount":false,"mask":false,"autofocus":false,'
                '"spellcheck":true,"disabled":false,"tableView":true,"modalEdit":false,"multiple":false,"persistent":'
                'true,"inputFormat":"plain","protected":false,"dbIndex":false,"case":"","encrypted":false,"redrawOn":'
                '"","clearOnHide":true,"customDefaultValue":"","calculateValue":"","calculateServer":false,'
                '"allowCalculateOverride":false,"validateOn":"change","validate":{"required":true,"pattern":"",'
                '"customMessage":"","custom":"","customPrivate":false,"json":"","minLength":"","maxLength":"",'
                '"strictDateValidation":false,"multiple":false,"unique":false},"unique":false,"errorLabel":"","key":'
                '"name","tags":[],"properties":{},"conditional":{"show":null,"when":null,"eq":"","json":""},'
                '"customConditional":"","logic":[],"attributes":{},"overlay":{"style":"","page":"","left":"","top":"",'
                '"width":"","height":""},"type":"textfield","input":true,"refreshOn":"","dataGridLabel":false,'
                '"inputType":"text","id":"evzldlun","defaultValue":""},{"label":"Achternaam","tableView":true,"key":'
                '"lastName","type":"textfield","input":true,"placeholder":"","prefix":"","customClass":"","suffix":"",'
                '"multiple":false,"defaultValue":null,"protected":false,"unique":false,"persistent":true,"hidden":'
                'false,"clearOnHide":true,"refreshOn":"","redrawOn":"","modalEdit":false,"labelPosition":"top",'
                '"description":"","errorLabel":"","tooltip":"","hideLabel":false,"tabindex":"","disabled":false,'
                '"autofocus":false,"dbIndex":false,"customDefaultValue":"","calculateValue":"","calculateServer":false,'
                '"widget":{"type":"input"},"attributes":{},"validateOn":"change","validate":{"required":false,"custom":'
                '"","customPrivate":false,"strictDateValidation":false,"multiple":false,"unique":false,"minLength":"",'
                '"maxLength":"","pattern":""},"conditional":{"show":null,"when":null,"eq":""},"overlay":{"style":"",'
                '"left":"","top":"","width":"","height":""},"allowCalculateOverride":false,"encrypted":false,'
                '"showCharCount":false,"showWordCount":false,"properties":{},"allowMultipleMasks":false,"mask":false,'
                '"inputType":"text","inputFormat":"plain","inputMask":"","spellcheck":true,"id":"een8nx9",'
                '"dataGridLabel":false}],"input":false,"placeholder":"","prefix":"","customClass":"","suffix":"",'
                '"multiple":false,"defaultValue":null,"protected":false,"unique":false,"persistent":false,"hidden":'
                'false,"clearOnHide":false,"refreshOn":"","redrawOn":"","tableView":false,"modalEdit":false,'
                '"labelPosition":"top","description":"","errorLabel":"","tooltip":"","hideLabel":false,"tabindex":"",'
                '"disabled":false,"autofocus":false,"dbIndex":false,"customDefaultValue":"","calculateValue":"",'
                '"calculateServer":false,"widget":null,"attributes":{},"validateOn":"change","validate":{"required":'
                'false,"custom":"","customPrivate":false,"strictDateValidation":false,"multiple":false,"unique":false},'
                '"conditional":{"show":null,"when":null,"eq":""},"overlay":{"style":"","left":"","top":"","width":"",'
                '"height":""},"allowCalculateOverride":false,"encrypted":false,"showCharCount":false,"showWordCount":'
                'false,"properties":{},"allowMultipleMasks":false,"tree":false,"theme":"default","breadcrumb":'
                '"default","id":"eypah2g","dataGridLabel":false},{"title":"Page 2","label":"Page 2","type":"panel",'
                '"key":"page2","components":[{"label":"Wat is uw vraag","labelPosition":"top","placeholder":"",'
                '"description":"","tooltip":"","prefix":"","suffix":"","widget":{"type":"input"},"editor":"",'
                '"autoExpand":false,"customClass":"","tabindex":"","autocomplete":"","hidden":false,"hideLabel":false,'
                '"showWordCount":false,"showCharCount":false,"autofocus":false,"spellcheck":true,"disabled":false,'
                '"tableView":true,"modalEdit":false,"multiple":false,"persistent":true,"inputFormat":"html",'
                '"protected":false,"dbIndex":false,"case":"","encrypted":false,"redrawOn":"","clearOnHide":true,'
                '"customDefaultValue":"","calculateValue":"","calculateServer":false,"allowCalculateOverride":false,'
                '"validateOn":"change","validate":{"required":false,"pattern":"","customMessage":"","custom":"",'
                '"customPrivate":false,"json":"","minLength":"","maxLength":"","minWords":"","maxWords":"",'
                '"strictDateValidation":false,"multiple":false,"unique":false},"unique":false,"errorLabel":"","key":'
                '"watIsUwVraag","tags":[],"properties":{},"conditional":{"show":null,"when":null,"eq":"","json":""},'
                '"customConditional":"","logic":[],"attributes":{},"overlay":{"style":"","page":"","left":"","top":"",'
                '"width":"","height":""},"type":"textarea","rows":3,"wysiwyg":false,"input":true,"refreshOn":"",'
                '"dataGridLabel":false,"allowMultipleMasks":false,"mask":false,"inputType":"text","inputMask":"",'
                '"fixedSize":true,"id":"exiwwsc","defaultValue":""}],"input":false,"placeholder":"","prefix":"",'
                '"customClass":"","suffix":"","multiple":false,"defaultValue":null,"protected":false,"unique":false,'
                '"persistent":false,"hidden":false,"clearOnHide":false,"refreshOn":"","redrawOn":"","tableView":false,'
                '"modalEdit":false,"dataGridLabel":false,"labelPosition":"top","description":"","errorLabel":"",'
                '"tooltip":"","hideLabel":false,"tabindex":"","disabled":false,"autofocus":false,"dbIndex":false,'
                '"customDefaultValue":"","calculateValue":"","calculateServer":false,"widget":null,"attributes":{},'
                '"validateOn":"change","validate":{"required":false,"custom":"","customPrivate":false,'
                '"strictDateValidation":false,"multiple":false,"unique":false},"conditional":{"show":null,"when":null,'
                '"eq":""},"overlay":{"style":"","left":"","top":"","width":"","height":""},"allowCalculateOverride":'
                'false,"encrypted":false,"showCharCount":false,"showWordCount":false,"properties":{},'
                '"allowMultipleMasks":false,"tree":false,"theme":"default","breadcrumb":"default","id":"ehcb46d"}]}',
                # type: ignore # noqa: E501 ^
    }

    # The user input
    form_input = {"tEst": "test", "name": f"{field_empty_value}", "lastName": "", "watIsUwVraag": ""}  # type: ignore # noqa: E501

    assert form_data is not None
    assert form_input is not None
    form = form_data.get("form", "")
    form_components: list = json.loads(form)["components"]

    validation_service = ValidationService()
    validation_service.errors = validation_service.Errors()
    validation_service.form_input_dict = form_input

    validation_service.validate_form_input_dict(form_components)
    assert isinstance(validation_service.errors, ValidationService.Errors)
    assert isinstance(validation_service.errors.get_errors(), list)
    assert isinstance(validation_service.errors.error_amount, int)

    assert len(validation_service.errors.get_errors()) == 1
    assert validation_service.errors.get_errors()[0] == "name is een verplicht veld."
    assert int(validation_service.errors.error_amount) == 1

    # TEst met meerdere unieke keys?
