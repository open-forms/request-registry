"""This validation_service module is used to validate the input for a request by comparing its formInput to the
settings of all properties (/components) in the form"""

import json
import re
import datetime
import ipaddress
from decimal import Decimal, DecimalException
import base64
import binascii
import numpy as np
import validators
import babel.numbers
from sqlalchemy.orm import Session
from validate_email import validate_email
from password_validation import PasswordPolicy
from app.services.commonground_service import get_resource
from app.schemas.request_schemas import RequestIn
from app.database import models


def return_bad_request(message: str, stack_type: str):
    """Function that creates a Bad Request response body to return"""
    return [{
      "name": "Bad Request",
      "message": message,
      "stack": {"type": stack_type}
    }]


class ValidationService:
    """The validationService class, this class is used to validate the formInput of a request"""
    def __init__(self):
        self.request: RequestIn = None  # the request input, set when doing a validate_request()
        self.database = None  # database Session for unique validation, set when doing a validate_request()
        self.errors: ValidationService.Errors = self.Errors()  # Errors class, (re)set when doing a validate_request()
        self.form_properties: list = []  # a list with the properties (/components) from the form given in the request,
        # set when doing a get_form_properties() (in the validate_request() function)
        self.form_input_dict: dict = {}  # a dictionary with the values from the formInput given in the request,
        # set when doing a create_form_input_dict() (in the validate_request() function)
        self.password_policy: PasswordPolicy = PasswordPolicy(lowercase=1, uppercase=1, symbols=1,
                                                              numbers=1, min_length=8)
        # a password policy for validating a password from the password_validation library.

    class Errors:
        """An errors class to store errors in while validating the formInput, so we can return them all at the
        end of validate_request()"""
        def __init__(self):
            self.errors: list = []
            self.error_amount: int = 0

        def add_error(self, message: str):
            """Adds an error to the list of errors"""
            self.errors.append(message)
            self.error_amount = self.error_amount + 1

        def get_errors(self):
            """Returns all the errors"""
            return self.errors

    def validate_request(self, request: RequestIn, database: Session):
        """Validates the formInput of a request and returns true or an error"""
        self.request = request  # (re)set request
        self.database = database  # set database Session
        error = self.get_form_properties()
        if isinstance(error, list):
            return error
        error = self.create_form_input_dict()
        if isinstance(error, list):
            return error
        self.errors = self.Errors()  # (re)set errors
        self.validate_form_input_dict()
        if self.errors.error_amount > 0:
            return [{
                "name": "Bad Request",
                "message": self.errors.get_errors(),
                "stack": {"type": "formInputValidation", "error_amount": self.errors.error_amount}
            }]
        return True

    def get_form_properties(self):
        """Creates a list with all form properties from the form json blob"""
        form = self.request.form
        # If request.form = an url than get the form from forms-catalogue
        if validators.url(form):
            # (use regex to validate the uuid) (this only works with urls with uuid, not with slugs!)
            regex_match = re.match(r"^http://formulieren.openformulieren.io/forms/([0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{"
                                   r"3}-[89ab][0-9a-f]{3}-[0-9a-f]{12})$", form)
            if regex_match:
                form_object = self.get_form_resource(form)
                if 'form' not in form_object:
                    return form_object
                # Get the actual form from the forms-catalogue object
                form_str = form_object.get('form')
            else:
                return return_bad_request(f"form: {form} is geen geldige url, geef een geldige url op voor een "
                                          f"formulier object: http://formulieren.openformulieren.io/forms/[uuid].",
                                          "invalidForm")
        else:
            # If form is not an url, it could be a json blob of a form.io form instead. Check if components is present
            # in the form, because this is the minimum requirement to continue validation for it.
            if 'components' not in form:
                if len(form) > 150:
                    form = "form"
                return return_bad_request(f"form: {form} is geen geldige url of geldige form.io form (json).",
                                          "invalidForm")
            form_str = form
#         print(form) # PRINT FOR DEBUGGING
        try:
            form_properties = json.loads(form_str)['components']
            if not isinstance(form_properties, list) or len(form_properties) == 0 or 'type' not in form_properties[0]:
                if len(form) > 150:
                    form = "\'form\'"
                return return_bad_request(f"{form} form.io form should at least contain one valid component in a list"
                                          f" of \"components\"", "invalidForm")
            self.form_properties = form_properties
            return None
        except ValueError:
            if len(form) > 150:
                form = "\'form\'"
            return return_bad_request(f"{form} form.io form is geen geldige json.", "invalidForm")

    @staticmethod
    def get_form_resource(form: str):
        """Tries to get a form resource from the forms-catalogue with a given form url"""

        form_object = get_resource(form, {}, False).json()
        if not isinstance(form_object, dict):
            return return_bad_request(f"form: {form} is geen bestaand formulier object in de forms-catalogue.",
                                      "invalidForm")
        # Make sure the status of the form is ACTIVE
        form_status = form_object.get('status')
        if form_status == 'INACTIVE':
            return return_bad_request(f"form: {form} kan niet ingediend worden. De status van het formulier "
                                      f"is ‘inactief’. Indien u het formulier in wilt kunnen dienen dient de "
                                      f"status door de beheerder veranderd te worden naar 'actief’.",
                                      "invalidForm")
        # Make sure the publishDate of the form has passed and isn't in the future
        form_publish_date = form_object.get('publishDate')
        if form_publish_date and form_publish_date > str(datetime.date.today()):
            return return_bad_request(f"form: {form} kan niet ingediend worden. Het formulier is nog "
                                      f"niet gepubliceerd.", "invalidForm")
        return form_object

    def create_form_input_dict(self):
        """Creates a normal dictionary with each property & value pair from the formInput json blob"""
        form_input = self.request.formInput
        # print(formInput) # PRINT FOR DEBUGGING
        try:
            self.form_input_dict = json.loads(form_input)
            return None
        except ValueError:
            if len(form_input) > 100:
                form_input = ""
            return return_bad_request(f"formInput {form_input} is geen geldige json.", "invalidFormInput")

    def validate_form_input_dict(self, form_properties: list = None):
        """Validates the values in the (formInput) dictionary with the properties from a form"""
        if form_properties is None:
            form_properties = self.form_properties
        layout_components = ['panel', 'fieldset', 'well']
        special_components = ['container', 'datagrid', 'editGrid']
        # loop through form properties and validate the values
        for component in form_properties:
            # print(component['key']) # PRINT FOR DEBUGGING
            if component['type'] in layout_components or component['type'] in special_components:
                self.validate_form_input_dict(component['components'])
            elif component['type'] == 'columns':
                for column in component['columns']:
                    self.validate_form_input_dict(column['components'])
            elif component['type'] == 'table':
                for row in component['rows']:
                    for column in row:
                        self.validate_form_input_dict(column['components'])
            else:
                self.validate_value(component)

    def validate_value(self, component: dict):
        """Validate a single component/property of the form with the input value"""
        # If the form component key is also present in the formInput
        if component['key'] in self.form_input_dict:
            value = self.form_input_dict.get(component['key'])
            # If value is not required and value = empty/null...
            if 'required' in component['validate'] and not component['validate']['required'] and not value:
                # ...and if validation nullable is not set, or if it is set but to nullable = false...
                if not ('nullable' in component['validate'] and not component['validate']['nullable']):
                    # ...skip the validation, because there is nothing we should try to validate!
                    return
            # do any additional validation for that component type:
            self.__component_type_switch(component, value)
            for validation in component['validate']:
                self.__validate_type_switch(validation, component, value)
        # If no input is present in the formInput for a form property that is required...
        elif 'required' in component['validate'] and component['validate']['required']:
            self.errors.add_error(str(component['key']) + ' is een verplicht veld.')
        elif 'nullable' in component['validate'] and not component['validate']['nullable']:
            self.errors.add_error(str(component['key']) + ' mag niet leeg (null) zijn.')

    def __component_type_switch(self, component: dict, value):
        """A switch for different component types"""
        switcher = {
            "textfield": self.__validate_text,
            "textarea": self.__validate_text,
            "number": self.__validate_number,
            "uuid": self.__validate_uuid,
            "email": self.__validate_email,
            "phoneNumber": self.__validate_phone_number,
            "date": self.__validate_day,
            "day": self.__validate_day,
            "time": self.__validate_time,
            "datetime": self.__validate_datetime,
            "month": self.__validate_month,
            "week": self.__validate_week,
            "password": self.__validate_password,
            "checkbox": self.__validate_check_box,
            "selectboxes": self.__validate_select_boxes,
            "select": self.__validate_select,
            "radio": self.__validate_radio,
            "currency": self.__validate_currency,
            "money": self.__validate_money,
            "file": self.__validate_attachment,
            "image": self.__validate_attachment,
            "color": self.__validate_color,
            "url": self.__validate_url,
            "float": self.__validate_float,
            "double": self.__validate_double,
            "integer": self.__validate_integer,
            "int32": self.__validate_int32,
            "int64": self.__validate_int64,
            "uri": self.__validate_uri,
            "hostname": self.__validate_hostname,
            "ipv4": self.__validate_ipv4,
            "ipv6": self.__validate_ipv6,
            "byte": self.__validate_byte,
            "binary": self.__validate_binary,
            "kenteken": self.__validate_kenteken,
            "bsn": self.__validate_bsn,
            "kvk": self.__validate_kvk,
            "rsin": self.__validate_rsin,
            "gemeente code": self.__validate_gemeente_code
        }
        # Get the function from switcher dictionary
        func = switcher.get(component['type'], None)
        # Execute the function
        if func is not None:
            func(component, value)

    def __validate_type_switch(self, validation: str, component: dict, value):
        """A switch for different validations"""
        switcher = {
            "unique": self.__validate_unique,
            "required": self.__validate_required,
            "nullable": self.__validate_nullable,
            "minLength": self.__validate_min_length,
            "maxLength": self.__validate_max_length,
            "min": self.__validate_min_value,
            "max": self.__validate_max_value,
            "exclusiveMinimum": self.__validate_exclusive_minimum,
            "exclusiveMaximum": self.__validate_exclusive_maximum,
            "multipleOf": self.__validate_multiple_of,
            "minItems": self.__validate_min_items,
            "maxItems": self.__validate_max_items,
            "uniqueItems": self.__validate_unique_items,
            "pattern": self.__validate_pattern,
        }
        # Get the function from switcher dictionary
        func = switcher.get(validation, None)
        # Execute the function
        if func is not None:
            func(component, value)

    def __validate_attachment_type_switch(self, key: str, value):
        """A switch for the different attachment input key"""
        switcher = {
            "storage": self.__validate_storage,
            "name": self.__validate_text,
            "url": self.__validate_base64,
            "size": self.__validate_number,
            "type": self.__validate_text,
            "originalName": self.__validate_text,
        }
        # Get the function from switcher dictionary
        # Key string looks something like this: "file: name" or "file: url" so we need to get the substring after ' '
        func = switcher.get(key[key.rindex(' ')+1:], None)
        # Execute the function
        if func is not None:
            if func in (self.__validate_text, self.__validate_number):
                func({'key': key}, value)
            else:
                func(key, value)

    def __validate_unique(self, component: dict, value):
        """Validate if a value is unique"""
        if component['validate']['unique']:
            # NOTE: if the request input body has an url for the form, this will only check requests with the exact
            # same url set as their form, so even if there is a request with a form set to a json blob that matches
            # the json blob of the form url, this will not count it as a request with the same form.

            component_property = component['key']
            # get all requests with the same form
            query = self.database.query(models.Request).filter(models.Request.form == self.request.form)
            result = self.database.execute(query).scalars().all()
            # than loop through all requests, get component['key'] value and check if that value matches the input value
            for request in result:
                # print(request.id) # PRINT FOR DEBUGGING
                try:
                    request_form_input_dict = json.loads(request.formInput)
                    if component_property in request_form_input_dict:
                        request_value = request_form_input_dict.get(component_property)
                        # print(requestValue) # PRINT FOR DEBUGGING
                        if request_value == value:
                            self.errors.add_error(str(component_property) + f' moet uniek zijn, er bestaat al een '
                                                                            f'verzoek met deze waarde: {value}.')
                            break
                except ValueError:
                    continue

    def __validate_required(self, component: dict, value):
        """Validation for required"""
        if component['validate']['required'] and not value:
            self.errors.add_error(str(component['key']) + ' is een verplicht veld.')

    def __validate_nullable(self, component: dict, value):
        """Validation for nullable"""
        if not ('required' in component['validate'] and component['validate']['required']) \
          and not component['validate']['nullable'] and (not value or value is None):
            self.errors.add_error(str(component['key']) + ' mag niet leeg (null) zijn.')

    def __validate_min_length(self, component: dict, value):
        """Validation for minLength"""
        if component['validate']['minLength'] and len(value) < int(component['validate']['minLength']):
            self.errors.add_error(str(component['key']) + ' is te kort, ' + str(component['key'])
                                  + ' heeft een minimum lengte van ' + str(component['validate']['minLength']) + '.')

    def __validate_max_length(self, component: dict, value):
        """Validation for maxLength"""
        if component['validate']['maxLength'] and len(value) > int(component['validate']['maxLength']):
            self.errors.add_error(str(component['key']) + ' is te lang, ' + str(component['key'])
                                  + ' heeft een maximum lengte van ' + str(component['validate']['maxLength']) + '.')

    def __validate_min_value(self, component: dict, value):
        """Validation for minValue"""
        if component['validate']['min'] and int(value) < int(component['validate']['min']):
            self.errors.add_error(str(component['key']) + ' is te laag, ' + str(component['key'])
                                  + ' heeft een minimum waarde van ' + str(component['validate']['min']) + '.')

    def __validate_max_value(self, component: dict, value):
        """Validation for maxValue"""
        if component['validate']['max'] and int(value) > int(component['validate']['max']):
            self.errors.add_error(str(component['key']) + ' is te hoog, ' + str(component['key'])
                                  + ' heeft een maximum waarde van ' + str(component['validate']['max']) + '.')

    def __validate_exclusive_minimum(self, component: dict, value):
        """Validation for exclusive minimum"""
        if component['validate']['exclusiveMinimum'] and int(value) <= int(component['validate']['exclusiveMinimum']):
            self.errors.add_error(str(component['key']) + ' is te laag, ' + str(component['key'])
                                  + ' heeft een exclusief minimum waarde van '
                                  + str(component['validate']['exclusiveMinimum']) + '.')

    def __validate_exclusive_maximum(self, component: dict, value):
        """Validation for exclusive maximum"""
        if component['validate']['exclusiveMaximum'] and int(value) >= int(component['validate']['exclusiveMaximum']):
            self.errors.add_error(str(component['key']) + ' is te hoog, ' + str(component['key'])
                                  + ' heeft een exclusief maximum waarde van '
                                  + str(component['validate']['exclusiveMaximum']) + '.')

    def __validate_multiple_of(self, component: dict, value):
        """Validation for multipleOf"""
        if not int(value) % int(component['validate']['multipleOf']) == 0:
            self.errors.add_error(str(component['key']) + ' is geen multipleOf 2.')

    def __validate_min_items(self, component: dict, value):
        """Validation for min items"""
        if component['validate']['minItems'] and len(value) < int(component['validate']['maxItems']):
            self.errors.add_error(str(component['key']) + ' heeft te wijnig items, ' + str(component['key'])
                                  + ' heeft een minimum van ' + str(component['validate']['minItems']) + ' items.')

    def __validate_max_items(self, component: dict, value):
        """Validation for max items"""
        if component['validate']['maxItems'] and len(value) > int(component['validate']['minItems']):
            self.errors.add_error(str(component['key']) + ' heeft te veel items, ' + str(component['key'])
                                  + ' heeft een maximum van ' + str(component['validate']['maxItems']) + ' items.')

    def __validate_unique_items(self, component: dict, value):
        """Validation for unique items"""
        if isinstance(value, list):
            if len(set(value)) != len(value):
                self.errors.add_error(str(component['key']) + f' {str(value)} de items zijn niet uniek.')

    def __validate_pattern(self, component: dict, value):
        """Validation for pattern"""
        # print(component['validate']['pattern']) # PRINTS FOR DEBUGGING
        # Make sure value is a of type string, cause if not = internal server error
        if not isinstance(value, str):
            self.errors.add_error(f'{str(component["key"])} verwacht een input van het type string '
                                  f'(niet {str(type(value)).replace("<class ", "").replace(">", "")}) om te '
                                  f'controleren of deze voldoet aan de volgende regular expression: '
                                  f'{str(component["validate"]["pattern"])}.')
        elif 'validate' in component and 'pattern' in component['validate'] and component['validate']['pattern']:
            regex_result = re.match(component['validate']['pattern'], value)
            if not regex_result:
                self.errors.add_error(f'{str(component["key"])} moet overeenkomen met de volgende regular expression: '
                                      f'{str(component["validate"]["pattern"])}.')

    def __validate_text(self, component: dict, value):
        """Validation for text, component type textfield and textarea"""
        # print(str(component["key"])); print(value) # PRINTS FOR DEBUGGING
        if not isinstance(value, str):
            self.errors.add_error(f'{str(component["key"])} verwacht een input van het type string, niet '
                                  f'{str(type(value)).replace("<class ", "").replace(">", "")}.')

    def __validate_number(self, component: dict, value):
        """Validation for component type number"""
        if not str(value).isnumeric():
            self.errors.add_error(str(component['key']) + f': {str(value)} is geen geldig nummer.')

    def __validate_uuid(self, component: dict, value):
        """Validation for component type UUID"""
        regex_result = re.match(r"^([0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12})$", value)
        if not regex_result:
            self.errors.add_error(str(component['key']) + f': {str(value)} is geen geldige UUID.')

    def __validate_email(self, component: dict, value):
        """Validation for component type email"""
        self.__validate_text(component, value)
        if not validate_email(value, verify=True):
            # email is not valid, exception
            self.errors.add_error(str(component['key']) + f': {str(value)} is geen geldig e-mailadres.')

    def __validate_phone_number(self, component: dict, value):
        """Validation for component type phoneNumber"""
        phone_number = re.sub("[^0-9+]", "", value)
        regex_result = re.match(r"^\+?[1-9]\d{1,14}$", phone_number)
        # another regex option for getting a E.164 phoneNumber (+ the specific land code):
        # \+?(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|
        # 5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\d{1,14}$
        if not regex_result:
            self.errors.add_error(str(component['key']) + f': {str(value)} is geen geldig telefoonnummer. Geef een '
                                                          f'geldig telefoonnummer dat voldoet aan de E.164 standaard.')

    def __validate_day(self, component: dict, value):
        """Validation for component type day and date"""
        day_str = value.replace('/', '-')
        try:
            datetime.strptime(str(day_str), '%d-%m-%Y')
        except ValueError:
            self.errors.add_error(str(component['key']) + f': {str(value)} is geen geldige datum, geef een geldige '
                                                          f'dag/datum op, die een d-m-Y formaat heeft.')

    def __validate_time(self, component: dict, value):
        """Validation for component type time"""
        try:
            datetime.strptime(str(value), '%H:%M')
        except ValueError:
            self.errors.add_error(str(component['key']) + f': {str(value)} is geen geldige tijd, geef een geldige tijd '
                                                          f'op, die een HH:mm formaat heeft.')

    def __validate_datetime(self, component: dict, value):
        """Validation for component type datetime in ISO 8601 format; 2021-08-03T07:40:51+00:00"""
        try:
            datetime.fromisoformat(str(value))
        except ValueError:
            self.errors.add_error(str(component['key']) + f': {str(value)} is geen geldige datum en tijd, '
                                                          f'geef een geldige datum en tijd op, dat voldoet aan '
                                                          f'de ISO 8601 format.')

    def __validate_month(self, component: dict, value):
        """Validation for component type month"""
        # checks if the value as an int is not between 1 and 12
        if int(value) < 1 or int(value) > 12:
            self.errors.add_error(str(component['key']) + f': {str(value)} is geen geldige maand.')

    def __validate_week(self, component: dict, value):
        """Validation for component type week"""
        # checks if the value as an int is not between 1 and 52
        if int(value) < 1 or int(value) > 52:
            self.errors.add_error(str(component['key']) + f': {str(value)} is geen geldige week.')

    def __validate_password(self, component: dict, value):
        """Validation for component type password"""
        if not self.password_policy.validate(value):
            self.errors.add_error(str(component['key']) + f': {str(value)} is geen geldig wachtwoord.')
            unfulfilled_requirements = self.password_policy.test_password(value)
            for unfulfilled_requirement in unfulfilled_requirements:
                # unfulfilledRequirement type = class RequirementUnfulfilled (has variables .name, .actual and
                # .requirement)
                self.errors.add_error(str(component['key']) + f': {unfulfilled_requirement}')

    def __validate_check_box(self, component: dict, value):
        """Validation for component type checkbox"""
        # Check if boolean (true/false) or string boolean ('true'/'false')
        if not isinstance(value, bool):
            if isinstance(value, str) and value.lower() in ['true', 'false']:
                value = True if value.lower() == 'true' else False
                # save this^ bool value in the actual request formInput for forminput->component['key'] = value
                # (instead of string version of the bool) ?
            else:
                self.errors.add_error(f'{str(component["key"])}: {str(value)} is geen geldige input voor een checkbox, '
                                      f'hier word een value van het type boolean verwacht (of string \'true\' / '
                                      f'\'false\'), niet {str(type(value)).replace("<class ", "").replace(">", "")}.')

    def __validate_select_boxes(self, component: dict, value):
        """Validation for component type select boxes"""
        if not isinstance(value, dict):
            self.errors.add_error(f'{str(component["key"])}: {str(value)} is geen geldige input voor selectboxes, '
                                  f'hier word een value van het type dict verwacht, niet '
                                  f'{str(type(value)).replace("<class ", "").replace(">", "")}.')
        else:
            # Check if the options in value match the options in the component values. When all selectboxes are set to
            # False we expect a value like = {\"value1\":false,\"value2\":false,\"value3\":false} but in this case
            # value will equal {'': False}, so also check for this if not all options match the component options.
            component_options = [option['value'] for option in component['values']]
            if set(value.keys()) != set(component_options) and value != {'': False}:
                self.errors.add_error(f'{str(component["key"])}: {str(value)} is geen geldige input, de gegeven opties '
                                      f'komen niet overeen met de opties van {str(component["key"])}')

    def __validate_select(self, component: dict, value):
        """Validation for component type select"""
        # Check if value is a list, if so check if multiple is set to true
        component_options = [option['value'] for option in component['data']['values']]
        # print(component_options)
        # print(value)
        if isinstance(value, list):
            if component['multiple']:
                # If value is a list & multiple is true, make sure all selected options are valid options
                if not set(value).issubset(set(component_options)):
                    self.errors.add_error(f'{str(component["key"])}: {str(value)} is geen geldige input, de gegeven '
                                          f'opties komen niet overeen met de opties van {str(component["key"])}')
            else:
                self.errors.add_error(f'{str(component["key"])}: {str(value)} is geen geldige input voor deze select, '
                                      f'hier word maar een geselecteerde optie verwacht.')
        elif isinstance(value, str):
            # If value is not a list but a string make sure the selected option is a valid option
            if value not in component_options:
                self.errors.add_error(f'{str(component["key"])}: {str(value)} is geen geldige input, de gegeven optie '
                                      f'komt niet overeen met een van de opties van {str(component["key"])}')
        else:
            self.errors.add_error(f'{str(component["key"])}: {str(value)} is geen geldige input voor een select, '
                                  f'hier word een value van het type list of str verwacht, niet '
                                  f'{str(type(value)).replace("<class ", "").replace(">", "")}.')

    def __validate_radio(self, component: dict, value):
        """Validation for component type radio"""
        # Make sure the value is of type string
        if not isinstance(value, str):
            self.errors.add_error(f'{str(component["key"])}: {str(value)} is geen geldige input voor een radio, '
                                  f'hier word een value van het type str verwacht, niet '
                                  f'{str(type(value)).replace("<class ", "").replace(">", "")}.')
        else:
            # Make sure it is one of the valid options
            component_options = [option['value'] for option in component['values']]
            if value not in component_options:
                self.errors.add_error(f'{str(component["key"])}: {str(value)} is geen geldige input, de gegeven optie '
                                      f'komt niet overeen met een van de opties van {str(component["key"])}')

    def __validate_currency(self, component: dict, value):
        """Validation for component type currency"""
        string_number = (str(value)).split(".")
        if len(string_number) != 2 or len(string_number[1]) != 2:
            self.errors.add_error(str(component['key']) + f': {str(value)} is geen geldige currency, '
                                                          f'verwacht 2 decimalen achter de comma.')

    def __validate_money(self, component: dict, value):
        """Validation for component type money. expect; ISO CODE | spatie | Integer"""
        string = (str(value)).split(" ")
        if len(string) == 2 and string[1].isnumeric():
            try:
                # babel.numbers.format_currency does not actually ever raise any exceptions !
                babel.numbers.format_currency(Decimal(string[1]), string[0])
                return
            except DecimalException:
                self.errors.add_error(str(component['key']) + f': {str(value)} is geen geldige currency, '
                                                              f'ISO CODE | spatie | Integer.')
        self.errors.add_error(str(component['key']) + f': {str(value)} is geen geldige currency, '
                                                      f'ISO CODE | spatie | Integer.')

    def __validate_attachment(self, component: dict, value):
        """Validation for component type file"""
        # if list:
        if isinstance(value, list):
            for file in value:
                self.__validate_file(component, file)
        else:
            self.errors.add_error(f'{str(component["key"])}: {str(value)} is geen geldige input voor een attachment, '
                                  f'hier word een value van het type list verwacht, niet '
                                  f'{str(type(value)).replace("<class ", "").replace(">", "")}.')

    def __validate_file(self, component: dict, file):
        # if dict:
        if isinstance(file, dict):
            # check dict op keys & values
            keys = ['storage', 'name', 'url', 'size', 'type', 'originalName']
            if all(x in file for x in keys):
                error_amount = self.errors.error_amount
                # for loop file
                for key in file.keys():
                    # A switch for the keys
                    self.__validate_attachment_type_switch(f'{str(component["key"])}: {key}', file.get(key))
                if self.errors.error_amount != error_amount:
                    if file['url'] and len(file['url']) > 30:
                        file['url'] = f'{str(file["url"][0:30])}...'
                    self.errors.add_error(str(component['key']) + f': {str(file)} is geen geldige file/image.')
            else:
                if file['url'] and len(file['url']) > 30:
                    file['url'] = f'{str(file["url"][0:30])}...'
                self.errors.add_error(str(component['key']) + f': {str(file)} is geen geldige file/image, omdat'
                                                              f' een van de volgende keys ontbreekt: {keys}')
        else:
            if file['url'] and len(file['url']) > 30:
                file['url'] = f'{str(file["url"][0:30])}...'
            self.errors.add_error(f'{str(component["key"])}: {str(file)} is geen geldige input voor een '
                                  f'file/image, hier word een value van het type dict verwacht, niet '
                                  f'{str(type(file)).replace("<class ", "").replace(">", "")}.')

    def __validate_color(self, component: dict, value):
        """Validation for component type color. Is validated on hex color code"""
        regex_result = re.match(r"^#(?:[0-9a-fA-F]{1,2}){3}$", value)
        if not regex_result:
            self.errors.add_error(str(component['key']) + f': {str(value)} is geen geldige hex kleur.')

    def __validate_url(self, component: dict, value):
        """Validation for component type url"""
        if not validators.url(str(value)):
            self.errors.add_error(str(component['key']) + f': {str(value)} is geen geldige url.')

    def __validate_float(self, component: dict, value):
        """Validation for component type float"""
        try:
            float(value)
        except ValueError:
            self.errors.add_error(str(component['key']) + f': {str(value)} is geen geldige float.')

    def __validate_double(self, component: dict, value):
        """Validation for component type double"""
        string_number = (str(value)).split(".")
        if len(string_number) != 2:
            self.errors.add_error(str(component['key']) + f': {str(value)} is geen geldige double.')

    def __validate_integer(self, component: dict, value):
        """Validation for component type integer"""
        try:
            int(value)
        except ValueError:
            self.errors.add_error(str(component['key']) + f': {str(value)} is geen geldige integer.')

    # TODO ? -> voor nu laten zitten - is niet getest
    def __validate_int32(self, component: dict, value):
        """Validation for component type int32"""
        if not np.int32(value):
            self.errors.add_error(str(component['key']) + f': {str(value)} is geen geldige int32.')

    # TODO ? -> voor nu laten zitten - is niet getest
    def __validate_int64(self, component: dict, value):
        """Validation for component type int64"""
        if not np.int64(value):
            self.errors.add_error(str(component['key']) + f': {str(value)} is geen geldige int64.')

    # TODO Only validates urls for now, not reversed dns uri's
    def __validate_uri(self, component: dict, value):
        """Validation for component type uri"""
        if not validators.url(str(value)):
            self.errors.add_error(str(component['key']) + f': {str(value)} is geen geldige uri. See https://www.w3.org/'
                                                          f'Addressing/URL/4_URI_Recommentations.html for more '
                                                          f'information.')

    def __validate_hostname(self, component: dict, value):
        """Validation for component type hostname"""
        if not validators.domain(value):
            self.errors.add_error(str(component['key']) + f': {str(value)} is geen geldige hostname.')

    def __validate_ipv4(self, component: dict, value):
        """Validation for component type ipv4"""
        try:
            ipaddress.IPv4Address(value)
        except ValueError:
            self.errors.add_error(str(component['key']) + f': {str(value)} is geen geldige ipv4.')

    def __validate_ipv6(self, component: dict, value):
        """Validation for component type ipv6"""
        try:
            ipaddress.IPv6Address(value)
        except ValueError:
            self.errors.add_error(str(component['key']) + f': {str(value)} is geen geldige ipv6.')

    def __validate_byte(self, component: dict, value):
        """Validation for component type byte"""
        regex_result = re.match(r"^[0-1]{8}$", value)
        if not regex_result:
            self.errors.add_error(str(component['key']) + f': {str(value)} is geen geldige byte.')

    def __validate_binary(self, component: dict, value):
        """Validation for component type binary"""
        regex_result = re.match(r"^([0-1]{8} ?)*$", value)
        if not regex_result or len(value.replace(" ", "")) % 8 != 0:
            self.errors.add_error(str(component['key']) + f': {str(value)} is geen geldige binary.')

    def __validate_kenteken(self, component: dict, value):
        """Validation for component type kenteken -- numbers and letters"""
        regex_result = re.match(r"^[a-zA-Z0-9-]*$", value)
        if not regex_result:
            self.errors.add_error(str(component['key']) + f': {str(value)} is geen geldige kenteken.')

    def __validate_bsn(self, component: dict, value):
        """Validation for component type bsn"""
        regex_result = re.match(r"^[0-9]{8,9}$", value)
        if not regex_result:
            self.errors.add_error(str(component['key']) + f': {str(value)} is geen geldige bsn.')

    def __validate_kvk(self, component: dict, value):
        """Validation for component type kvk"""
        regex_result = re.match(r"^[0-9]{12}$", value)
        if not regex_result:
            self.errors.add_error(str(component['key']) + f': {str(value)} is geen geldige kvk.')

    def __validate_rsin(self, component: dict, value):
        """Validation for component type rsin"""
        regex_result = re.match(r"^[A-Z]{2}[0-9]{9}[B][0-9]{2}$", value)
        if not regex_result:
            self.errors.add_error(str(component['key']) + f': {str(value)} is geen geldige rsin.')

    def __validate_gemeente_code(self, component: dict, value):
        """Validation for component type gemeentecode"""
        regex_result = re.match(r"^[0-9]{1,4}$", value)
        if not regex_result:
            self.errors.add_error(str(component['key']) + f': {str(value)} is geen geldige gemeentecode.')

    def __validate_base64(self, key: str, value):
        """Validation for attachment-url"""
        string = (str(value)).split(',')
        try:
            base64.b64encode(base64.b64decode(string[1])) == string[1]
        except binascii.Error:
            self.errors.add_error(str(key) + f': {str(string[1])} is geen geldige base64.')

    def __validate_storage(self, key: str, value):
        """Validation for attachment-storage"""
        if not value == 'base64':
            self.errors.add_error(str(key) + f': {str(value)} is geen geldige storage optie.')
