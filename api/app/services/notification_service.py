"""This notification_service module is used to notify the notification component using the commonground_service"""

from enum import Enum
import os
from app.services.commonground_service import create_resource


class NotifyActions(Enum):
    """A enum for the possible notification actions"""
    CREATE = "Create"
    UPDATE = "Update"
    DELETE = "Delete"


def notify(notification_type: str, action: NotifyActions, uuid: str):
    """Notify the notification component with type, a action and for a given object (uuid)"""

    notification = {
        "topic": "request-registry/" + notification_type,
        "resource": os.environ.get('APP_URL', '') + "/" + notification_type + "/" + uuid,
        "action": action.value
    }
    if os.environ.get('APP_URL', '') != "http://localhost":
        response = create_resource(notification, {'component': 'notification-component', 'type': 'notifications'},
                                   None, True)
    else:
        response = create_resource(notification, {'component': 'nrc', 'type': 'notifications'})
    # print(response.text)  # PRINT FOR DEBUGGING
    return response.text
