"""This commonground_service module is used to communicate with other commonground components / api's"""

import json
import os
import requests as apiRequests
import validators

defaultHeaders = {
  'Authorization': os.environ.get('APP_APPLICATION_KEY', ''),
  'Content-Type': 'application/json'
}


def create_resource(body: dict, url, headers: dict = None, intern: bool = False):
    """Create api call to a commonground component"""

    if isinstance(url, dict):
        url_dict = url
        domain = os.environ.get('APP_DOMAIN', '')
        if 'domain' in url_dict:
            domain = url_dict['domain']
        url = "http://" + domain + "/v1/" + url_dict['component'] + "/"+url_dict['type']
        if intern:
            url = "http://" + url_dict['component'] + "." + os.environ.get('APP_ENV', '') + ".svc.cluster.local/" \
                  + url_dict['type']
    if not isinstance(url, str) or not validators.url(url):
        return False
#     print(url) # PRINT FOR DEBUGGING
    payload = json.dumps(body)
    if headers is None:
        headers = defaultHeaders
    return apiRequests.request("POST", url, headers=headers, data=payload)


def get_resource(url, headers: dict = None, intern: bool = False):
    """Get api call to a commonground component"""

    if isinstance(url, dict):
        url_dict = url
        domain = os.environ.get('APP_DOMAIN', '')
        if 'domain' in url_dict:
            domain = url_dict['domain']
        url = "http://" + domain+"/v1/" + url_dict['component'] + "/"+url_dict['type'] + "/"+url_dict['id']
        if intern:
            url = "http://" + url_dict['component'] + "."+os.environ.get('APP_ENV', '') + ".svc.cluster.local/" \
                  + url_dict['type'] + "/"+url_dict['id']
    if not isinstance(url, str) or not validators.url(url):
        return False
#     print(url) # PRINT FOR DEBUGGING
    if headers is None:
        headers = defaultHeaders
    return apiRequests.request("GET", url, headers=headers, data={})
