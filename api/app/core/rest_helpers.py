"""
Helper functions for "JSON Server Data Provider For React-Admin" API requests

The JSON Server Data Provider For React-Admin style of API is defined at
https://github.com/marmelab/react-admin/tree/master/packages/ra-data-json-server
"""

from typing import Optional, TypeVar, Union
from fastapi import Query
from loguru import logger
from pydantic import BaseModel, ValidationError
from app.core.types import (
    RangeOptions,
    SemanticError,
    SemanticErrorType,
    SortOptions,
)

FieldsType = TypeVar("FieldsType", bound=BaseModel)


def parse_filter(
    filter_: Optional[str],
    filter_options_schema: Optional[FieldsType] = None,
):
    """Not implemented yet"""
    return None


def parse_range(
    start_: Optional[str], end_: Optional[str]
) -> Optional[RangeOptions]:
    """Parse range"""

    if not start_:
        return None

    if not end_:
        return None

    logger.debug(f"range start: {start_}, range end: {end_}")

    return RangeOptions(start=start_, end=end_)


def parse_sort(
    sort_: Optional[str],
    order_: Optional[str],
    sort_options_schema: Optional[FieldsType] = None,
) -> Optional[SortOptions]:
    """Parse sort"""

    if not sort_:
        return None

    if not order_:
        return None

    if not sort_options_schema:
        raise ValueError("No sort schema. Add a sort schema.")

    logger.debug(f"sort field: {sort_}, sort order: {order_}")

    field = sort_
    order = order_

    # to sort on a join it is possible to add the Model_name to the sort field
    # For example to sort contact_moment on Employee add Employee.name
    # use capital E on Employee
    model_name: Union[str, None] = None
    if field.count(".") == 1:
        model_name, field = field.split(".")
    sort_dict = {field: order}
    try:
        sort_options = sort_options_schema(**sort_dict).dict(  # type: ignore
            exclude_unset=True
        )
    except ValidationError as exception:
        raise SemanticError(
            loc={"source": "path", "field": "id"},
            msg=exception.errors(),
            type=SemanticErrorType.INVALID_INPUT,
        ) from exception

    field = list(sort_options.keys())[0]
    order = sort_options[field]
    return SortOptions(field=field, order=order, model_name=model_name)


class SearchParameters:
    """Class for the search parameters"""
    def __init__(
        self,
        filter_options_schema: Optional[FieldsType] = None,
        sort_options_schema: Optional[FieldsType] = None,
    ):
        """Init"""
        self.filter_options_schema = filter_options_schema
        self.sort_options_schema = sort_options_schema

    def __call__(
        self,
        sort_: Optional[str] = Query(
            None,
            alias="_sort",
            description="Sort order. The field to sort by.",
        ),
        order_: Optional[str] = Query(
            None,
            alias="_order",
            description="Sort order. The sort order (ASC or DESC).",
        ),
        start_: Optional[str] = Query(
            None,
            alias="_start",
            description="Range of items to return. Range start.",
        ),
        end_: Optional[str] = Query(
            None,
            alias="_end",
            description="Range of items to return. Range end.",
        ),
        filter_: Optional[str] = None,
    ) -> dict:
        """Search Parameters"""

        return {
            "sort_options": parse_sort(
                sort_, order_, self.sort_options_schema
            ),
            "range_options": parse_range(start_, end_),
            "filter_options": parse_filter(
                filter_, self.filter_options_schema
            ),
        }
