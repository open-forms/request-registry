#! /usr/bin/env bash

if [ -d .venv ]; then
    echo ".venv already exists. Remove it first if you want to recreate it." >&2
    exit 1
fi

if [ ! -e api/requirements.txt ]; then
    echo "api/requirements.txt does not exist." >&2
    echo "Are you running this from the project root?" >&2
fi

echo "Creating virtual environment in .venv"
python3 -m venv .venv
PATH=.venv/bin:"$PATH"
echo "Upgrading 'pip'"
pip3 install --upgrade pip
echo "Installing requirements"
pip3 install -r api/requirements.txt