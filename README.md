# Request Registry

The request registry holds form results or submitted forms

## Features

- **FastAPI** with Python 3.8
- PostgreSQL
- SqlAlchemy with Alembic for migrations
- Docker compose for easier development
- Pytest for backend tests

## Development

The only dependencies for this project should be docker and docker-compose.

## Visual code extensions

The following extensions are needed/useful

- Pylance
- Python

### Quick Start

First time build the containers

```bash
docker-compose build
```

Starting the project with hot-reloading enabled (the first time it will take a while):

```bash
docker-compose up -d
```

To run the alembic migrations (to get your database schema up to date):

```bash
docker-compose run --rm requests-api alembic upgrade head
```

Relevant URLs: TODO

### Rebuilding containers:

```bash
docker-compose build
```

### Restarting containers:

```bash
docker-compose restart
```

### Bringing containers down:

```bash
docker-compose down
```

## Migrations

Migrations are run using alembic. To run all migrations:

```bash
docker-compose run --rm requests-api alembic upgrade head
```

To load initial data:

```bash
docker-compose run --rm requests-api python3 app/initial_data.py
```

To create a new migration, first define the table in `app.database.models` and then run:

```bash
docker-compose run --rm requests-api alembic revision --autogenerate -m "describe change here"
```

This will generate an update script for Alembic. Double-check if it does what
it should, adjust as needed, and add it to your commit. The upgrade/downgrade
is described in the `upgrade` and `downgrade` methods of a newly created file
in `api/app/alembic/versions`.

For more information see
[Alembic's official documentation](https://alembic.sqlalchemy.org/en/latest/tutorial.html#create-a-migration-script).

## Testing

```bash
docker-compose run --rm requests-api pytest
```

any arguments to pytest can also be passed after this command. For instance:

```bash
docker-compose run --rm requests-api pytest -vvsx
```

This runs the tests in extra verbose mode (-v -v), stops at the first error
(-x) and shows anything that was printed to STDOUT (-s). Usefull arguments are:

Get rid of annoying lib warnings written the end of the tests.

```bash
docker-compose run --rm requests-api pytest -vvsx --disable-warnings
```

Run tests in specific file.

```bash
docker-compose run requests-api pytest /app/<path_of_test_file>/<test_file_name> -vvsx --disable-warnings

For example:
docker-compose run requests-api pytest app/api/api_v1/routers/tests/test_post_request.py -vvsx --disable-warnings
```

Run specific test in specific file.

```bash
docker-compose run requests-api pytest /app/<path_of_test_file>/<test_file_name> -vvsx --disable-warnings -k <test_name>

For example:
docker-compose run requests-api pytest app/api/api_v1/routers/tests/test_post_request.py -vvsx --disable-warnings -k test_post_request_valid_form_value
```

## Project Layout

```
api
└── app
    ├── alembic
    │   └── versions # where migrations are located
    ├── api
    │   └── api_v1
    │       └── routers   # endpoints
    │           └── tests # pytest
    ├── core    # config
    ├── db      # db models
    ├── domain  # domain
    ├── schemas # schemas
    └── main.py # entrypoint to api

app
└──

dev
└── db        # Script to initialize PostgreSQL databases on first start
```
